#include "caliConfig.h"
#include "getData.h"
#include "fitTemplate.h"
#include "createXML.h"
#include "templateXML.h"

#include "ROOT/TSeq.hxx"
#include <sstream>
using namespace std;

int main(int argc , char **argv){

  if (argc > 2) {
    std::cout<<"only the first 2 arguments are taken into account: "<<std::endl;
  }
  if (argc < 1) {
    std::cout<<"main called with only "<<argc<<" arguments; quit"<<std::endl;
    return 1;
  }

  //argv[1]
  //VCalib_lj1
  //VCalib_lj2
  //ZCalib_lj1
  //ZCalib_lj2
  TString _doWhichCalib = argv[1];

  //argv[2]
  //doFit
  //doXML
  //doBoth
  TString _doWhat =argv[2];


  if(_doWhichCalib.Contains("VCalib")){//config for V+jets calib
    std::cout<< "Do V+jets calibration" << std::endl;
    caliConfig::_Calib = _doWhichCalib;
    caliConfig::_doFit = false;
    caliConfig::_InputDir = "/sps/atlas/y/yahe/hadd/";
    caliConfig::_TreeName = "Vtag";
    if(_doWhichCalib.Contains("lj2")){
      caliConfig::_Category = "lj2";
      std::cout << "Do subleading large-R channel" << std::endl;
    }else{
      caliConfig::_Category = "lj1";
      std::cout<< "Do leading large-R channel" << std::endl;
    }
    caliConfig::_FitOutputDir = "/sps/atlas/y/yahe/Vjets-fit/templateFit/output/";
    caliConfig::_XMLOutputDir = "/sps/atlas/y/yahe/Vjets-fit/templateFit/xml/";

    //initialize process
    caliConfig::_runProcess.push_back("wqq");
    caliConfig::_runProcess.push_back("zqq");
    caliConfig::_runProcess.push_back("ttbarCatTop");
    caliConfig::_runProcess.push_back("ttbarCatW");
    caliConfig::_runProcess.push_back("ttbarCatOther");


    //initialize pTBins
    caliConfig::_pTBins.push_back("500-600");
    caliConfig::_pTBins.push_back("600-700");
    caliConfig::_pTBins.push_back("700-800");
    caliConfig::_pTBins.push_back("800-1000");
    caliConfig::_pTBins.push_back("1000-1500");
    caliConfig::_pTCuts.push_back(500.);
    caliConfig::_pTCuts.push_back(600.);
    caliConfig::_pTCuts.push_back(700.);
    caliConfig::_pTCuts.push_back(800.);
    caliConfig::_pTCuts.push_back(1000.);
    caliConfig::_pTCuts.push_back(1500.);

  }else if(_doWhichCalib.Contains("ZCalib")){ //config for Zbb+jets calib
    std::cout<< "Do Zbb+jets calibration" << std::endl;
    caliConfig::_Calib = "ZCalib";
    caliConfig::_doFit = false;
    caliConfig::_InputDir = "/sps/atlas/y/yahe/hadd-zbb/";
    caliConfig::_TreeName = "Ztag";
    if(_doWhichCalib.Contains("lj2")){
      caliConfig::_Category = "lj2";
      std::cout << "Do subleading large-R channel" << std::endl;
    }else{
      caliConfig::_Category = "lj1";
      std::cout<< "Do leading large-R channel" << std::endl;
    }
    caliConfig::_FitOutputDir = "/sps/atlas/y/yahe/Vjets-fit/templateFit/output/";
    caliConfig::_XMLOutputDir = "/sps/atlas/y/yahe/Vjets-fit/templateFit/xml/";
  
    //initialize process
    caliConfig::_runProcess.push_back("zbb");
    // caliConfig::_runProcess.push_back("zqq"); don't consider the contribution of Zqq and Wqq for now
    // caliConfig::_runProcess.push_back("wqq");
    caliConfig::_runProcess.push_back("ttbarCatTop");
    caliConfig::_runProcess.push_back("ttbarCatWOther"); //combine W and Other as one


    //initialize pTBins
    caliConfig::_pTBins.push_back("450-1000");
    caliConfig::_pTCuts.push_back(450.);
    caliConfig::_pTCuts.push_back(1000.);
    /*
    caliConfig::_pTBins.push_back("450-500");
    caliConfig::_pTBins.push_back("500-600");
    caliConfig::_pTBins.push_back("600-700");
    caliConfig::_pTBins.push_back("700-1000");
    caliConfig::_pTCuts.push_back(450.);
    caliConfig::_pTCuts.push_back(500.);
    caliConfig::_pTCuts.push_back(600.);
    caliConfig::_pTCuts.push_back(700.);
    caliConfig::_pTCuts.push_back(1000.);
    */
  }else{
    std::cout<<"Unknown Setup, please intervene; quit"<<std::endl;
    return 1;
  }
  
  //initialize systematics
  caliConfig::initSystematics();

  for (int j=0;j<caliConfig::_pTBins.size();j++){
    map<TString, double> nominal_yields;
    if(_doWhat == "doXML" || _doWhat == "doBoth"){

      std::cout << "==================pt: " << caliConfig::_pTBins[j] << "GeV========================" << std::endl;
      nominal_yields = getData::getyields(caliConfig::_pTCuts[j],caliConfig::_pTCuts[j+1]);
      for(int ip=0;ip<caliConfig::_runProcess.size();ip++){
	std::cout << caliConfig::_runProcess[ip] << ": " << nominal_yields[caliConfig::_runProcess[ip]] << std::endl;
      }
      std::cout << "==================================================================================" << std::endl;

    }
    //templateXML::getSglsystXML(caliConfig::_pTBins[j],"nominal",nominal_yields,"1");
    

    if(_doWhat == "doFit" || _doWhat == "doBoth"){ //fit template
    
      for (auto isyst : ROOT::TSeqI(caliConfig::_Systematics.size())){
	//if(!caliConfig::_Systematics[isyst].Contains("nominal")) continue;
	if(caliConfig::_Systematics[isyst].Contains("nominal") || caliConfig::_Systematics[isyst].Contains("sh228")){
	  templateXML::getSglFit(caliConfig::_pTBins[j],caliConfig::_Systematics[isyst]);
	}else{
	  templateXML::getSglFit(caliConfig::_pTBins[j],caliConfig::_Systematics[isyst]+"__1down");
	  templateXML::getSglFit(caliConfig::_pTBins[j],caliConfig::_Systematics[isyst]+"__1up");
	}
      }
    }
  
    
    if(_doWhat == "doXML" || _doWhat == "doBoth"){ //create xml  

      TString whichorder = "1";
      if(caliConfig::_Calib.Contains("VCalib")){
	if(caliConfig::_pTBins[j].Contains("500-600") || caliConfig::_pTBins[j].Contains("600-700")){
	  whichorder = "4";
	}else if(caliConfig::_pTBins[j].Contains("700-800") || caliConfig::_pTBins[j].Contains("800-1000")){
	  whichorder = "3";
	}else if(caliConfig::_pTBins[j].Contains("1000-1500")){
	  whichorder = "3";
	}
      }
      templateXML::getAllSystXML(caliConfig::_pTBins[j],nominal_yields,whichorder);
    }

  } //close pT bins
  return 0;
}
