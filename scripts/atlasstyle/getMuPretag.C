#include "Rtypes.h"

#include "AtlasUtils.h"
#include "AtlasStyle.h"
#include "AtlasLabels.h"

#ifdef __CLING__
// these are not headers - do not treat them as such - needed for ROOT6
#include "AtlasLabels.C"
#include "AtlasUtils.C"
#endif
#include <TGraph.h>
#include <TGraphErrors.h>
#include <TLatex.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TFile.h>
#include <TString.h>
#include <TProfile.h>
#include <TH1D.h>
#include <TH1F.h>
#include <TF1.h>
#include <TF2.h>
#include <TLine.h>
#include <vector>
#include <iostream>

TString allSysall[] = {"mp8","weight_leptonSF_EL_SF_Trigger_UP",		      "weight_leptonSF_EL_SF_Trigger_DOWN",
		      		      "weight_leptonSF_EL_SF_Reco_UP",		      "weight_leptonSF_EL_SF_Reco_DOWN",
		       "weight_leptonSF_EL_SF_ID_UP",		      "weight_leptonSF_EL_SF_ID_DOWN",
		      "weight_leptonSF_EL_SF_Isol_UP",		      "weight_leptonSF_EL_SF_Isol_DOWN",
		      "weight_leptonSF_MU_SF_Trigger_STAT_UP","weight_leptonSF_MU_SF_Trigger_STAT_DOWN",
		      "weight_leptonSF_MU_SF_Trigger_SYST_UP","weight_leptonSF_MU_SF_Trigger_SYST_DOWN",
		       "weight_leptonSF_MU_SF_ID_STAT_UP","weight_leptonSF_MU_SF_ID_STAT_DOWN",
		       "weight_leptonSF_MU_SF_ID_SYST_UP","weight_leptonSF_MU_SF_ID_SYST_DOWN","weight_leptonSF_MU_SF_ID_STAT_LOWPT_UP","weight_leptonSF_MU_SF_ID_STAT_LOWPT_DOWN","weight_leptonSF_MU_SF_ID_SYST_LOWPT_UP","weight_leptonSF_MU_SF_ID_SYST_LOWPT_DOWN","weight_leptonSF_MU_SF_Isol_STAT_UP","weight_leptonSF_MU_SF_Isol_STAT_DOWN","weight_leptonSF_MU_SF_Isol_SYST_UP","weight_leptonSF_MU_SF_Isol_SYST_DOWN","weight_leptonSF_MU_SF_TTVA_STAT_UP","weight_leptonSF_MU_SF_TTVA_STAT_DOWN","weight_leptonSF_MU_SF_TTVA_SYST_UP","weight_leptonSF_MU_SF_TTVA_SYST_DOWN","weight_pileup_UP","weight_pileup_DOWN","EG_RESOLUTION_ALL__1down","EG_RESOLUTION_ALL__1up","EG_SCALE_AF2__1down","EG_SCALE_AF2__1up","EG_SCALE_ALL__1down","EG_SCALE_ALL__1up","MUON_ID__1down","MUON_ID__1up","MUON_MS__1down","MUON_MS__1up","MUON_SAGITTA_RESBIAS__1down","MUON_SAGITTA_RESBIAS__1up","MUON_SAGITTA_RHO__1down","MUON_SAGITTA_RHO__1up","MUON_SCALE__1down","MUON_SCALE__1up",
"CategoryReduction_JET_EffectiveNP_R10_Mixed1__1down",
  "CategoryReduction_JET_EffectiveNP_R10_Mixed1__1up",
		       "CategoryReduction_JET_EffectiveNP_R10_Mixed2__1down",
		       "CategoryReduction_JET_EffectiveNP_R10_Mixed2__1up",
		       "CategoryReduction_JET_EffectiveNP_R10_Modelling1__1down",
		       "CategoryReduction_JET_EffectiveNP_R10_Modelling1__1up",
		       "CategoryReduction_JET_EffectiveNP_R10_Modelling3__1down",
		       "CategoryReduction_JET_EffectiveNP_R10_Modelling3__1up",
		       "CategoryReduction_JET_EtaIntercalibration_Modelling__1down",
		       "CategoryReduction_JET_EtaIntercalibration_Modelling__1up",
		       "CategoryReduction_JET_Flavor_Composition__1down",
		       "CategoryReduction_JET_Flavor_Composition__1up",
		       "CategoryReduction_JET_Flavor_Response__1down",
		       "CategoryReduction_JET_Flavor_Response__1up",
};

TH1D* readhist(TString filepath){
  TFile *of = TFile::Open(filepath);
  std::cout << filepath << std::endl;
  TTree *tt = (TTree*)of->Get("TwoLep");
  //tt->Draw("pTV>>h0(30,450,1200)","weight*(pTV>=450&&pTV<1200)");
  // tt->Draw("EtaV>>h0(25,-2.5,2.5)","weight*(pTV>=450&&pTV<1200)");
  //tt->Draw("PhiV>>h0(40,-4.0,4.0)","weight*(pTV>=450&&pTV<1200)");
  tt->Draw("mll>>h0(50,66,116)","weight*(pTV>=450&&pTV<500&&pTV>lj1_pt&&dYVJ<1.2&&aymVJ<0.15&&(((mu1_pt-mu2_pt)/pTV<0.8)&&((e1_pt-e2_pt)/pTV<0.8)))");
  //tt->Draw("mll>>h0(50,66,116)","weight*(pTV>=450&&pTV<1000&&pTV>lj1_pt&&dYVJ<1.2&&aymVJ<0.15&&mu1_pt>0&&((mu1_pt-mu2_pt)/pTV<0.8))");
  //tt->Draw("_eloss>>h0(30,450,1200)","weight*(pTV>=450&&pTV<1200)");
  TH1D * h0 = (TH1D*)gDirectory->Get("h0");
  h0->SetDirectory(0);
  h0->SetStats(0);
  h0->SetTitle("");
  of->Close();
  return h0;
}
TH1F* readhistall(TString sysName){
  TH1D* h1_0 = readhist("/sps/atlas/y/yahe/hadd-202104-lep/Zll-"+sysName+".root"); //zll
  TH1D* h2_0;
  TH1D* h3_0;
  if(sysName.Contains("MUR") || sysName.Contains("mp8")){
    h2_0 =readhist("/sps/atlas/y/yahe/hadd-202104-lep/ZZll-nominal.root");//ZZll
    h3_0 = readhist("/sps/atlas/y/yahe/hadd-202104-lep/WZll-nominal.root");//WZll
  }else{
    h2_0 =readhist("/sps/atlas/y/yahe/hadd-202104-lep/ZZll-"+sysName+".root");//ZZll
    h3_0 = readhist("/sps/atlas/y/yahe/hadd-202104-lep/WZll-"+sysName+".root");//WZll
  }
  
  TH1F *hall = (TH1F*)h1_0->Clone("hall");
  hall->Sumw2();
  hall->SetStats(0);      // No statistics on lower plot
  hall->Add(h2_0);
  hall->Add(h3_0);
  hall->SetDirectory(0);
  return hall;
}
TH1F* getSystBandup(TH1F* h_n){
  TH1F* hSystBand = new TH1F("up", "up;X up;Y up", 50,66,116); 
  //auto h_n = readhistall("nominal");
  
  for (TString sysName : allSysall) {
    if(sysName.Contains("_DOWN")||sysName.Contains("__1down")) continue;
    auto h_syst = readhistall(sysName);
    h_syst->Add(h_n,-1);
    TH1F *temp = (TH1F*)h_syst->Clone("temp");
    h_syst->Multiply(temp);
    hSystBand->Add(h_syst);
  }

  return hSystBand;
}

TH1F* getSystBanddn(TH1F* h_n){
  TH1F* hSystBand = new TH1F("dn", "dn;X dn;Y dn", 50,66,116);
  //auto h_n = readhistall("nominal");

  for (TString sysName : allSysall) {
    if(sysName.Contains("_UP")||sysName.Contains("__1up")) continue;
    auto h_syst = readhistall(sysName);
    h_syst->Add(h_n,-1);
    TH1F *temp = (TH1F*)h_syst->Clone("temp");
    h_syst->Multiply(temp);
    hSystBand->Add(h_syst);
  }
  return hSystBand;
}

TH1F* getSystBand(TH1F* h_all_n){  
  // auto h_all_n = readhistall("nominal");
  auto h_all_syst_up = getSystBandup(h_all_n);
  auto h_all_syst_dn = getSystBanddn(h_all_n);
  for (int ibin =1; ibin <= h_all_n->GetNbinsX(); ibin++){
    float stat =  h_all_n->GetBinError(ibin);
    float val = h_all_n->GetBinContent(ibin);
    float allup = sqrt(stat*stat+0.017*0.017*val*val+h_all_syst_up->GetBinContent(ibin));
    float alldn = sqrt(stat*stat+0.017*0.017*val*val+h_all_syst_dn->GetBinContent(ibin));
    h_all_n->SetBinError(ibin, max(allup,alldn));
  }
  return h_all_n;
}


void getMuPretag(){

  //gROOT->SetBatch(kTRUE);
    #ifdef __CINT__
  gROOT->LoadMacro("./AtlasLabels.C");
  gROOT->LoadMacro("./AtlasUtils.C");
  #endif


  // auto systband = getSystBand();

  SetAtlasStyle();
  TString allSys[] = {"nominal"};

  std::map<TString, Double_t> results;
  TString _pTbin = "450-500";
  TString _channel = "Zll";

  //gSystem->Exec("rm sysTxt-fullrun2-"+_pTbin+_channel+".txt");
  // gSystem->Exec("mkdir -p ./plot/"+_pTbin);
  Double_t nominalval;
  TH1D* h4 = readhist("/sps/atlas/y/yahe/hadd-202104-lep/data.root");
  for (TString sysName : allSys) { 
       TH1D* h1 = readhist("/sps/atlas/y/yahe/hadd-202104-lep/Zll-"+sysName+".root"); //zll
       TH1D* h2;
       TH1D* h3;
       if(sysName.Contains("MUR") || sysName.Contains("mp8")){
	 h2 =readhist("/sps/atlas/y/yahe/hadd-202104-lep/ZZll-nominal.root");//ZZll
	 h3 = readhist("/sps/atlas/y/yahe/hadd-202104-lep/WZll-nominal.root");//WZll
       }else{
	 h2 =readhist("/sps/atlas/y/yahe/hadd-202104-lep/ZZll-"+sysName+".root");//ZZll
	 h3 = readhist("/sps/atlas/y/yahe/hadd-202104-lep/WZll-"+sysName+".root");//WZll
       }

       TH1F *hh_n = (TH1F*)h1->Clone("hh_n");

       hh_n->Sumw2();
       hh_n->SetStats(0);      // No statistics on lower plot
       hh_n->Add(h2);
       hh_n->Add(h3);

       TH1F *hh_mp8 = readhistall("mp8");
       hh_mp8->Add(hh_n,-1);

       auto systband = getSystBand(hh_n);
       TH1F *systband_exp = (TH1F*)systband->Clone("systband_exp");
       for (int ibin =1; ibin <= systband_exp->GetNbinsX(); ibin++){
	 float err_all =  systband->GetBinError(ibin);
	 float err_mp8 = hh_mp8->GetBinContent(ibin);
	 systband_exp->SetBinError(ibin, sqrt(err_all*err_all-err_mp8*err_mp8));
       }

       TCanvas* c = new TCanvas("ratio plot 1/2",sysName,800,800);
       gStyle->SetErrorX(0.5);
       TPad *pad1 = new TPad("pad1", "pad1", 0, 0.3, 1, 1.0);
       pad1->SetBottomMargin(0.02); // Upper and lower plot are joined
       //pad1->SetGridx();         // Vertical grid
       //  pad1->SetLogy();
       pad1->Draw();             // Draw the upper pad: pad1
       pad1->cd();

       //pad1->DrawFrame(systband->GetXaxis()->GetXmin(), ->GetMinimum(),
       //	     h1->GetXaxis()->GetXmax(), h1->GetMaximum());

       THStack *hs = new THStack("hs","");
       h3->SetLineColor(kYellow-9);
       h3->SetFillColor(kYellow-9);
       hs->Add(h3);
       h2->SetLineColor(kGreen-9);
       h2->SetFillColor(kGreen-9);
       hs->Add(h2);
       h1->SetLineColor(kBlue-9);
       h1->SetFillColor(kBlue-9);
       hs->Add(h1);

       hs->Draw("hist");

       /*       TH1F *hh_n = (TH1F*)h1->Clone("hh_n");

       hh_n->Sumw2();
       hh_n->SetStats(0);      // No statistics on lower plot
       hh_n->Add(h2);
       hh_n->Add(h3);
       auto systband = getSystBand(hh_n);   
       */
       systband->SetFillColor(kBlack);
       systband->SetLineColor(kBlack);
       systband->SetMarkerColor(kBlack);
       systband->SetFillStyle(1001);
       systband->SetLineWidth(2);
       systband->SetMarkerSize(0.);

       //systband->SetLineWidth(2);
       //systband->SetFillColor(kGray);
       //systband->SetLineColor(kGray);
       //systband->SetFillStyle(3004);
       //systband->SetMarkerSize(0);
       //systband->Draw("same E2");

       float max = h1->GetMaximum();

       hs->GetXaxis()->SetTitle("m_{#ell#ell} [GeV]");
       hs->GetYaxis()->SetTitle("Events");
       hs->GetYaxis()->SetTitleOffset(2.0) ;
       hs->GetYaxis()->SetTitleSize(25) ;
       hs->SetMaximum(max*1.5);
       hs->SetMinimum(0.);
       h4->SetLineWidth(2);
       h4->SetLineColor(kBlack);
       h4->SetMarkerStyle(20);
       h4->Draw("same ep") ;

       TH1F *d1 = (TH1F*)h1->Clone("d1");

       d1->Sumw2();
       d1->SetStats(0);      // No statistics on lower plot
       d1->Add(h2);
       d1->Add(h3);
       std::cout << "KS: " <<  h4->KolmogorovTest(d1) << std::endl;
       std::cout << "Chi2: " << h4->Chi2Test(d1,"UW") << std::endl;
       TH1F *d2 = (TH1F*)systband->Clone("d2");
       d2->Divide(systband);
       d2->SetMinimum(0.0);  // Define Y ..
       d2->SetMaximum(2.2); // .. range
       d2->SetLineWidth(2);
       d2->SetFillColor(kRed-9);
       d2->SetLineColor(kRed-9);
       d2->SetFillStyle(1001);
       d2->SetMarkerSize(0);
       
       TH1F *d4 = (TH1F*)systband_exp->Clone("d4");
       d4->Divide(systband_exp);
       d4->SetMinimum(0.0);  // Define Y ..
       d4->SetMaximum(2.2); // .. range
       d4->SetLineWidth(2);
       d4->SetFillColor(kRed-7);
       d4->SetLineColor(kRed-7);
       d4->SetFillStyle(1001);
       d4->SetMarkerSize(0);

       TH1F *d3 = (TH1F*)d1->Clone("d2");
       d3->Divide(d1);
       d3->SetMinimum(0.0);  // Define Y ..
       d3->SetMaximum(2.2); // .. range
       d3->SetLineWidth(2);
       d3->SetFillColor(kRed-3);
       d3->SetLineColor(kRed-3);
       d3->SetFillStyle(1001);
       d3->SetMarkerSize(0);


       TH1F *bkg = (TH1F*)h4->Clone("bkg");
       bkg->Sumw2();
       bkg->SetStats(0);      // No statistics on lower plot
       bkg->Add(h2,-1);
       bkg->Add(h3,-1);
       Double_t error_num;
       Double_t num = bkg->IntegralAndError(1, bkg->GetNbinsX(), error_num, ""); // "" or "width"
       std::cout << "num = " << num << " +- " <<error_num << std::endl;
       Double_t error_den;
       Double_t den = h1->IntegralAndError(1, h1->GetNbinsX(), error_den, ""); // "" or "width"
       std::cout << "den = " << den << " +- " <<error_den << std::endl;
       std::cout<< "NF = " << num/den << "+-" << (error_num*den-error_den*num)/den/den << std::endl;
       results["val"] = num/den;
       results["error"] = (error_num*den-error_den*num)/den/den;
       if(sysName=="nominal") nominalval = num/den;
       results["nom"] = nominalval;
       for (auto& b : results){
	 std::cout<<Form("%15s",b.first.Data())<<"\t=\t"<<b.second<<std::endl;
       }
       bkg->Divide(h1);

       Double_t error_NF;
       Double_t NF = bkg->IntegralAndError(1, bkg->GetNbinsX(), error_NF, ""); // "" or "width"
       //std::cout << "NF = " << NF/bkg->GetNbinsX() << " +- " <<error_NF/bkg->GetNbinsX() << std::endl;
       TLegend * leg = new TLegend(0.25,0.70,0.55,0.90);
       //leg->SetNDC();
       leg->SetTextFont(43);
       leg->SetTextSize(25);
       leg->SetBorderSize(0);
       leg->SetMargin(0.3);
       if(_channel == "Zee"){
	 leg->AddEntry(h1,"Z#rightarrow ee sh228","f");
	 leg->AddEntry(h2,"ZqqZee","f");
	 leg->AddEntry(h3,"WqqZee","f");
       }else if(_channel == "Zuu"){
	 leg->AddEntry(h1,"Z#rightarrow uu sh228","f");
	 leg->AddEntry(h2,"ZqqZuu","f");
	 leg->AddEntry(h3,"WqqZuu","f");
       }else if(_channel == "Zll"){
	 leg->AddEntry(h1,"Z+jets","f");
	 leg->AddEntry(h2,"ZZ","f");
	 leg->AddEntry(h3,"WZ","f");
	 //leg->AddEntry(h1,"Z(#rightarrow #font[12]{l^{+}l^{-}})","f");
         //leg->AddEntry(h2,"Z(#rightarrow #font[12]{q#bar{q}})Z(#rightarrow #font[12]{l^{+}l^{-}})","f");
         //leg->AddEntry(h3,"W(#rightarrow #font[12]{qq'})Z(#rightarrow #font[12]{l^{+}l^{-}})","f");
       }
       leg->AddEntry(h4,"Data","lp");
       leg->Draw();
       TLegend * leg1 = new TLegend(0.45,0.73,0.85,0.88);
       //leg->SetNDC();
       leg1->SetTextFont(43);
       leg1->SetTextSize(25);
       leg1->SetBorderSize(0);
       leg1->SetMargin(0.23);
       leg1->AddEntry(d3,"Stat. unc.","f");
       leg1->AddEntry(d4,"Stat.+Exp. unc.","f");
       leg1->AddEntry(d2,"Stat.+Exp.+Z modelling unc.","f");
       leg1->Draw();
       
       ATLASLabel(0.20,0.50,"Internal");
       //ATLASLabel(0.2,0.2,"Work in progress");

       myText(       0.20,  0.44, 1, "#sqrt{s}= 13 TeV, 139 fb^{-1}");
       myText(       0.20, 0.38, 1, "Z(#rightarrow #font[12]{b#bar{b}})+jets calibration");
       myText(       0.20, 0.32, 1, "Z(#rightarrow #font[12]{l^{+}l^{-}})+jets");
       //myText(       0.2, 0.44, 1,"#epsilon_{Xbb}(v1) = 60%");
       myText(       0.20, 0.26, 1,"p_{T}: "+_pTbin+" GeV");

       TLatex *test = new TLatex();
       test->SetNDC();
       test->SetTextColor(kBlack);
       test->SetTextFont(42);
       test->SetTextSize(0.040);
       //test->DrawLatex(0.15,0.85,"#bf{#it{ATLAS Internal}},139 fb^{-1}");
       //test->DrawLatex(0.15,0.80,sysName);
       //test->DrawLatex(0.15,0.75,"Z+jets calibration");
       //test->DrawLatex(0.45,0.85,Form("%1d < mass < %1d GeV",50,150));
       //test->DrawLatex(0.45,0.80,Form("#bf{%1d #leq  p_{T}^{ll}} < %1d GeV",450,1000));
  //test->DrawLatex(0.45,0.75,"rescaled dijets = dijets mc16a*0.0+mc16d*0.0");
       //test->DrawLatex(0.15,0.70,_pTbin+ " GeV");
       //test->DrawLatex(0.2,0.60,"        #chi^{2}        KS");
       //test->DrawLatex(0.2,0.55,Form("Stat \t %.3f \t %.3f",h4->Chi2Test(d1,"UW"),h4->KolmogorovTest(d1)));

       c->cd();          // Go back to the main canvas before defining pad2
       TPad *pad2 = new TPad("pad2", "pad2", 0, 0.05, 1, 0.3);
       pad2->SetTopMargin(0.02);
       pad2->SetBottomMargin(0.3);
       pad2->SetGridy();
       //pad2->SetGridx();
       //pad2->SetLogy();
       pad2->Draw();
       pad2->cd();       // pad2 becomes the current pad
       SetAtlasStyle();  
       d2->Draw("E2");
       d4->Draw("same E2");
       d3->Draw("same E2");
       //d1->Divide(h4);
       bkg->SetMinimum(0.0);  // Define Y ..
       bkg->SetMaximum(2.2); // .. range
       bkg->SetLineWidth(2);
       bkg->SetFillColor(0);
       bkg->SetLineColor(kBlack);
       bkg->SetMarkerStyle(20);
       bkg->Draw("same p");       // Draw the ratio plot

       // Ratio plot (h3) settings
       bkg->SetTitle(""); // Remove the ratio title
       // Y axis ratio plot settings
       d2->GetYaxis()->SetTitle("(Data-Bkg)/Sig");

       // Y axis h1 plot settings
       hs->SetTitle("");
       hs->GetYaxis()->SetTitleSize(25);
       hs->GetYaxis()->SetTitleFont(43);
       hs->GetYaxis()->SetTitleOffset(2.0);
       hs->GetYaxis()->SetLabelSize(25);
       hs->GetYaxis()->SetLabelFont(43);
       hs->GetYaxis()->SetLabelOffset(0.01);
       hs->GetXaxis()->SetLabelSize(0);
       hs->GetXaxis()->SetTitleSize(0);

       d2->GetYaxis()->SetNdivisions(505);
       d2->GetYaxis()->SetTitleSize(25);
       d2->GetYaxis()->SetTitleFont(43);
       d2->GetYaxis()->SetTitleOffset(1.8);
       d2->GetYaxis()->SetLabelFont(43); // Absolute font size in pixel (precision 3)
       d2->GetYaxis()->SetLabelSize(25);
       d2->GetYaxis()->SetLabelOffset(0.01);

       // X axis ratio plot settings
       //d1->SetXTitle("p_{T}^{V}/GeV");
       //d1->GetXaxis()->SetTitle("#eta^{V}");
       //d1->GetXaxis()->SetTitle("#phi^{V}");
       d2->GetXaxis()->SetTitle("m_{#font[12]{ll}} [GeV]");
       d2->GetXaxis()->SetTitleSize(25);
       d2->GetXaxis()->SetTitleFont(43);
       d2->GetXaxis()->SetTitleOffset(4.0);
       d2->GetXaxis()->SetLabelFont(43); // Absolute font size in pixel (precision 3)
       d2->GetXaxis()->SetLabelSize(25);
       c->cd();
       c->SaveAs("2L"+sysName+_channel+_pTbin+"_v3.png");
       c->SaveAs("2L"+sysName+_channel+_pTbin+"_v3.pdf");
       c->SaveAs("2L"+sysName+_channel+_pTbin+"_v3.eps");
       h1->Clear();
       h2->Clear();
       h3->Clear();
  }
}
