#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class caliConfig+;
#pragma link C++ class getData+;
#pragma link C++ class fitTemplate+;
#pragma link C++ class createXML+;
#pragma link C++ class templateXML+;

#endif
