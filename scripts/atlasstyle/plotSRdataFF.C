#include "Rtypes.h"

#include "AtlasUtils.h"
#include "AtlasStyle.h"
#include "AtlasLabels.h"

#ifdef __CLING__
// these are not headers - do not treat them as such - needed for ROOT6
#include "AtlasLabels.C"
#include "AtlasUtils.C"
#endif

#include "RooRealVar.h"
#include "RooConstVar.h"
#include "RooGaussian.h"
#include "RooArgusBG.h"
#include "RooAddPdf.h"
#include "RooDataSet.h"
#include "RooAbsData.h"
#include "RooPlot.h"

#include "TH1.h"
#include "TH1D.h"


using namespace RooFit;
double binWidth = 1;
double mmax = 150;
double mmin = 50;
int nbins = (mmax-mmin)/binWidth;
int gofbin = 100;
int m_rebin = 100/gofbin;

map<TString, double> calcChi2(TH1* hdata, TH1* hpdf, double blindMin, double blindMax);
double drawplot(TString filename,TString _Calib, TString _ptbin,TString _func);
void plotSRdataSig(TString _Calib,TString _ptbin,TString _func);

void plotSRdataFF(){
  //plotSRdataSig("ZCalib","450-1000","poly3");
  plotSRdataSig("ZCalib","500-600","poly3");
  plotSRdataSig("ZCalib","450-500","poly3");
  plotSRdataSig("ZCalib","600-1000","expoly3");
}


void plotSRdataSig(TString _Calib,TString _ptbin,TString _func) {
  //TString _Calib = "ZCalib";
  //TString _ptbin = "450-1000";
  //TString _func = "poly3"
  TString filename1 = "../../xml/202104_FF_V3/"+_Calib+"/"+_ptbin+"/fitResult/res_"+_func+"_unbinned";
  //TString filename2 = "../xml/"+_Calib+"/"+_ptbin+"/fitResult/res3";
  double chi2_1 = drawplot(filename1, _Calib,_ptbin,_func);
  //double chi2_3 = drawplot(filename2, _Calib,_ptbin);

  //std::cout << "================== ML===============" << std::endl;
  //std::cout << "#Delta#tilde{#chi}^{2}_{31} = " <<  -2*(chi2_3 - chi2_1) << " p-value = "  << ROOT::Math::chisquared_cdf_c(-2*(chi2_3 - chi2_1), 2)*100. << "%" << std::endl;
  /*std::cout << "#Delta#tilde{#chi}^{2}_{32} = " <<  -2*(chi2_3_ml - chi2_2_ml) << " p-value = "  << ROOT::Math::chisquared_cdf_c(-2*(chi2_3_ml - chi2_2_ml), 1)*100. << "%" << std::endl;
  std::cout << "#Delta#tilde{#chi}^{2}_{43} = " <<  -2*(chi2_4_ml - chi2_3_ml) << " p-value = "  << ROOT::Math::chisquared_cdf_c(-2*(chi2_4_ml - chi2_3_ml), 1)*100. << "%" << std::endl;
  std::cout << "#Delta#tilde{#chi}^{2}_{54} = " <<  -2*(chi2_5_ml - chi2_4_ml) << " p-value = "  << ROOT::Math::chisquared_cdf_c(-2*(chi2_5_ml - chi2_4_ml), 1)*100. << "%" << std::endl;
  std::cout << "#Delta#tilde{#chi}^{2}_{65} = " <<  -2*(chi2_6_ml - chi2_5_ml) << " p-value = "  << ROOT::Math::chisquared_cdf_c(-2*(chi2_6_ml - chi2_5_ml), 1)*100. << "%" << std::endl;
  std::cout << "================== Chi2===============" << std::endl;*/
  //std::cout << "#Delta#tilde{#chi}^{2}_{21} = " <<  -2*(chi2_2_chi2 - chi2_1_chi2) << " p-value = "  << ROOT::Math::chisquared_cdf_c(abs(2*(chi2_2_chi2 - chi2_1_chi2)), 1)*100. << "%" << std::endl;
  //std::cout << "#Delta#tilde{#chi}^{2}_{32} = " <<  -2*(chi2_3_chi2 - chi2_2_chi2) << " p-value = "  << ROOT::Math::chisquared_cdf_c(abs(2*(chi2_3_chi2- chi2_2_chi2)), 1)*100. << "%" << std::endl;
  //std::cout << "#Delta#tilde{#chi}^{2}_{43} = " <<  -2*(chi2_4_chi2 - chi2_3_chi2) << " p-value = "  << ROOT::Math::chisquared_cdf_c(abs(2*(chi2_4_chi2- chi2_3_chi2)), 1)*100. << "%" << std::endl;
  //std::cout << "#Delta#tilde{#chi}^{2}_{54} = " <<  -2*(chi2_5_chi2 - chi2_4_chi2) << " p-value = "  << ROOT::Math::chisquared_cdf_c(abs(2*(chi2_5_chi2- chi2_4_chi2)), 1)*100. << "%" << std::endl;
 }

double drawplot(TString filename, TString _Calib, TString _ptbin, TString _func) {
    #ifdef __CINT__
  gROOT->LoadMacro("./AtlasLabels.C");
  gROOT->LoadMacro("./AtlasUtils.C");
  #endif

  SetAtlasStyle();
  
  // read data from root file
  // plot it
  TCanvas * can = new TCanvas("can_"+filename,"can_"+filename,800,800);

  TFile *file = new TFile(filename+".root");
  // getting the workspace
  RooWorkspace *w = (RooWorkspace*)file->Get("combWS");
  RooFitResult *result = (RooFitResult*)file->Get("fitResult");
  TH2* hcorr = result->correlationHist() ;

  int nfloatpara = result->floatParsFinal().getSize();
  // getting data
  RooDataSet* wdata = (RooDataSet*) w->data("combData");
  // getting observable
  RooRealVar *mass = (RooRealVar*)w->var("mass");
  RooRealVar *mu_V = (RooRealVar*)w->var("mu");
  
  RooAddPdf* modelSB = (RooAddPdf*)w->pdf("_modelSB_Full");
  //RooDataHist* binneddata = new RooDataHist("binneddata", "binneddata", RooArgSet(*mass), *wdata, 1.0); 
  RooDataHist* binneddata = wdata->binnedClone();
  std::cout << "================== RooDataSet===============" << std::endl;
  wdata->Print("v");
  std::cout<< "unbinned numEntries: "<< wdata->numEntries() <<std::endl;
  std::cout << "================== RooDataHist===============" << std::endl;
  binneddata->Print("v");
  std::cout<< "binned numEntries: "<< binneddata->numEntries() <<std::endl;

  std::cout << "================== chi2 test and chi2 test===============" << std::endl;
  TH1D* hdata = new TH1D("hdata","hdata",gofbin, mass->getMin(), mass->getMax());
  hdata->Sumw2();
  RooArgSet* obs_tmp = const_cast<RooArgSet*>(wdata->get());
  RooRealVar* xdata_tmp = dynamic_cast<RooRealVar*>(obs_tmp->first()); // We only have one observable in total, so it is okay
  for (int i=0 ; i<wdata->numEntries() ; i++) {
    wdata->get(i) ;
    mass->setVal(xdata_tmp->getVal());
    hdata->Fill(mass->getVal(),wdata->weight());
  }
  //hdata->Rebin(m_rebin);
  const int obsNBins = hdata->GetNbinsX();
  mass->setBins(obsNBins);
  auto* hpdf = (TH1D*) modelSB->createHistogram("hpdf", *mass);
  //hpdf->Rebin(m_rebin);
  hpdf->Scale(modelSB->expectedEvents(RooArgSet(*mass))/hpdf->Integral());
  //std::cout<< "expectedEvents: "<< modelSB->expectedEvents(RooArgSet(*mass)) <<std::endl;
  for( int ibin = 1 ; ibin <= obsNBins; ibin ++ ) hpdf->SetBinError(ibin, 0);
  std ::cout << "hdata bins " << hdata->GetNbinsX() << std::endl;
  std ::cout <<"hpdf bins " << hpdf->GetNbinsX() << std::endl;
  map<TString, double> chi2Res = calcChi2(hdata, hpdf,0,0);
  double chi2 = chi2Res["chi2"];
  int nbin_chi2 = nearbyint(chi2Res["nbinchi2"]);
  double nll = chi2Res["nll"], nllsat = chi2Res["nllsat"];
  int nbin_nll = nearbyint(chi2Res["nbinnll"]);
  
  std::cout << "nbin_chi2: " << nbin_chi2 << std::endl;
  std::cout << "nbin_nll: " << nbin_nll << std::endl;
 
  if( nbin_chi2 - nfloatpara > 0 ){
    double prob_chi2 = ROOT::Math::chisquared_cdf_c(chi2, nbin_chi2 - nfloatpara-1);
    std::cout << "#chi^{2}/ndof = " << chi2 << "/" << (nbin_chi2 - nfloatpara) << " = " << chi2/(nbin_chi2 - nfloatpara)  << " p-value = "  << prob_chi2*100. << "%" << std::endl;
  }
  if( nbin_nll - nfloatpara > 0 ){
    double prob_nll = ROOT::Math::chisquared_cdf_c(2*(nll-nllsat), nbin_nll - nfloatpara);
    std::cout << "2#DeltaNLL/ndof = " << "2*" << (nll-nllsat) << "/" << (nbin_nll - nfloatpara) << " = " << 2*(nll-nllsat)/(nbin_nll - nfloatpara) << " p-value = "  << prob_nll*100. << "%" << std::endl;
    }


  std::cout << "================== draw plot===============" << std::endl;

  TPad *pad1 = new TPad("pad2", "pad2", 0, 0.25, 1, 1.0);
  pad1->cd();
  pad1->SetLeftMargin(0.15);
  pad1->SetTicks();
  // can->Divide(1,2);
  //can->cd(1);
  //gPad->SetLeftMargin(0.15) ;
  RooPlot* xframe = mass->frame();
  wdata->plotOn(xframe,Name("data"),Binning(nbins,mass->getMin(), mass->getMax()),DrawOption("PZ"),MarkerSize(0.9),XErrorSize(0.));
  modelSB->plotOn(xframe,Name("Wqq"),Components("pdf__wqq_Full"),LineStyle(ELineStyle::kDashed),LineColor(kGreen),LineWidth(3));
  modelSB->plotOn(xframe,Name("Zqq"),Components("pdf__zqq_Full"),LineStyle(ELineStyle::kDashed),LineColor(kOrange),LineWidth(3));
  modelSB->plotOn(xframe,Name("ttbarCatTop"),Components("pdf__ttbarCatTop_Full"),LineStyle(ELineStyle::kDashed),LineColor(kCyan),LineWidth(3));
  if(filename.Contains("VCalib")) modelSB->plotOn(xframe,Name("ttbarCatW"),Components("pdf__ttbarCatW_Full"),LineStyle(ELineStyle::kDashed),LineColor(kViolet+6),LineWidth(3));
  if(filename.Contains("VCalib")) modelSB->plotOn(xframe,Name("ttbarCatOther"),Components("pdf__ttbarCatOther_Full"),LineStyle(ELineStyle::kDashed),LineColor(kViolet),LineWidth(3));
  if(filename.Contains("ZCalib")) modelSB->plotOn(xframe,Name("ttbarCatWOther"),Components("pdf__ttbarCatWOther_Full"),LineStyle(ELineStyle::kDashed),LineColor(kViolet),LineWidth(3));
  modelSB->plotOn(xframe,Name("qcd"),Components("pdf__bkgPdf_Full"),LineStyle(ELineStyle::kDashed),LineColor(kBlue),LineWidth(3));
  modelSB->plotOn(xframe,Name("model"),LineColor(kRed),LineWidth(3));
  // draw frame
  //xframe->BuildLegend();
  xframe->SetMaximum(xframe->GetMaximum()*1.15);
  xframe->GetYaxis()->SetLabelFont(43); // Absolute font size in pixel (precision 3)
  xframe->GetYaxis()->SetLabelSize(25);
  xframe->GetYaxis()->SetTitleOffset(1.5);
  xframe->GetYaxis()->SetLabelOffset(0.01);
  xframe->GetXaxis()->SetLabelFont(43); // Absolute font size in pixel (precision 3)
  xframe->GetXaxis()->SetLabelSize(0);
  //xframe->GetXaxis()->SetLabelOffset(0.008);
  //xframe->GetXaxis()->SetTitleOffset(1.0);
  xframe->SetXTitle("");
  xframe->SetYTitle("Events");
  xframe->SetTitle("");
  xframe->Draw();
  // save frame for further investigation
  //SetAtlasStyle();
  TLatex * bkg = new TLatex();
  bkg->SetNDC();
  bkg->SetTextFont(42);
  bkg->SetTextColor(1);
  bkg->SetTextSize(0.04);
  //if(filename.Contains("ZCalib")) bkg->DrawLatex(0.2,0.55,_func);
  RooRealVar* floatpara;
  TLatex *finalfloatpara = new TLatex();
  finalfloatpara->SetNDC();
  finalfloatpara->SetTextColor(kBlack);
  finalfloatpara->SetTextFont(42);
  finalfloatpara->SetTextSize(0.03);
  floatpara = (RooRealVar*) result->floatParsFinal().find("c");
  //  finalfloatpara->DrawLatex(0.65,0.30,Form("a1 :  %1.3f #pm %1.3f",floatpara->getVal(),floatpara->getError()));
  if(filename.Contains("VCalib") || filename.Contains("ZCalib")){
  floatpara = (RooRealVar*) result->floatParsFinal().find("d");
  //finalfloatpara->DrawLatex(0.65,0.25,Form("a2 :  %1.3f #pm %1.3f",floatpara->getVal(),floatpara->getError()));
  }
  if((filename.Contains("VCalib") || filename.Contains("ZCalib")) && !filename.Contains("1000-1500")){
  floatpara = (RooRealVar*) result->floatParsFinal().find("e");
  //finalfloatpara->DrawLatex(0.65,0.20,Form("a3 :  %1.3f #pm %1.3f",floatpara->getVal(),floatpara->getError()));
  }
  if(filename.Contains("VCalib") && filename.Contains("600")){
  floatpara = (RooRealVar*) result->floatParsFinal().find("f");
  //finalfloatpara->DrawLatex(0.65,0.15,Form("a4 :  %1.3f #pm %1.3f",floatpara->getVal(),floatpara->getError()));
  }  
  TLatex *ptbins = new TLatex();
  ptbins->SetNDC();
  ptbins->SetTextColor(kBlack);
  ptbins->SetTextFont(42);
  ptbins->SetTextSize(0.040);
  //ptbins->DrawLatex(0.2,0.45," p_{T}: "+_ptbin+"GeV");
  /* if(filename.Contains("450-500")) ptbins->DrawLatex(0.2,0.45,Form("%1d #leq p_{T} < %1d GeV",450,500));
  if(filename.Contains("500-600")) ptbins->DrawLatex(0.2,0.45,Form("%1d #leq p_{T} < %1d GeV",500,600));
  if(filename.Contains("600-700")) ptbins->DrawLatex(0.2,0.45,Form("%1d #leq p_{T} < %1d GeV",600,700));
  if(filename.Contains("700-800")) ptbins->DrawLatex(0.2,0.45,Form("%1d #leq p_{T} < %1d GeV",700,800));
  if(filename.Contains("700-1000")) ptbins->DrawLatex(0.2,0.45,Form("%1d #leq p_{T} < %1d GeV",700,1000));
  if(filename.Contains("800-1000")) ptbins->DrawLatex(0.2,0.45,Form("%1d #leq p_{T} < %1d GeV",800,1000));
  if(filename.Contains("1000-1500")) ptbins->DrawLatex(0.2,0.45,Form("%1d #leq p_{T} < %1d GeV",1000,1500));*/
  //ptbins->DrawLatex(0.2,0.40,Form("N^{obs} = %1.0f",wdata->sumEntries()));
  floatpara = (RooRealVar*) result->floatParsFinal().find("mu");
  //ptbins->DrawLatex(0.2,0.30,Form("#mu = %1.3f"/* _{-%1.3f}^{+ %1.3f}*/,floatpara->getVal()/*,floatpara->getAsymErrorLo(),floatpara->getAsymErrorHi()*/));
  //floatpara = (RooRealVar*) result->floatParsFinal().find("yield_bkg");
  //ptbins->DrawLatex(0.2,0.25,Form("N_{bkg,fit} = %1.0f #pm %1.0f",floatpara->getVal(),floatpara->getError()));
  
  /* TLatex *chiSquareText = new TLatex();
  chiSquareText->SetNDC();
  chiSquareText->SetTextColor(kBlack);
  chiSquareText->SetTextFont(42);
  chiSquareText->SetTextSize(0.035);
  if( nbin_chi2 - nfloatpara > 0 ){
  double prob_chi2 = ROOT::Math::chisquared_cdf_c(chi2, nbin_chi2 /*- nfloatpara*///);
/*    chiSquareText->DrawLatex(0.2,0.30,Form("#chi^{2}/ndof = %1.2f, p-value = %.2f %%", chi2/(nbin_chi2 - nfloatpara), prob_chi2*100.));
  }
  if( nbin_nll - nfloatpara > 0 ){
  double prob_nll = ROOT::Math::chisquared_cdf_c(2*(nll-nllsat), nbin_nll/* - nfloatpara*///);
/*  chiSquareText->DrawLatex(0.2,0.25,Form("#tilde{#chi}^{2}/ndof = %1.2f, p-value = %.2f %%", 2*(nll-nllsat)/(nbin_nll - nfloatpara), prob_nll*100.));
    }*/

  TLegend *leg1 = new TLegend(0.60,0.55,0.80,0.92);
  leg1->SetTextFont(43);
  leg1->SetTextSize(25);
  leg1->SetBorderSize(0);
  leg1->SetMargin(0.3);
  leg1->SetFillColor(kWhite);
  leg1->SetLineColor(kWhite);
  leg1->AddEntry("data","data", "P");
  leg1->AddEntry("model","fit","L");
  leg1->AddEntry("Zqq","fit_Z", "L");
  leg1->AddEntry("qcd","fit_qcd", "L");
  leg1->AddEntry("Wqq","fit_W", "L");
  leg1->AddEntry("ttbarCatTop","fit_t#bar{t}(tqqb)", "L");
  if(filename.Contains("ZCalib")) leg1->AddEntry("ttbarCatWOther","fit_t#bar{t}(no tqqb)", "L");
  if(filename.Contains("VCalib")) leg1->AddEntry("ttbarCatW","t#bar{t}(W)", "L");
  if(filename.Contains("VCalib")) leg1->AddEntry("ttbarCatOther","t#bar{t}(Other)", "L");
  leg1->Draw();


  TLatex * ra = new TLatex();
  ra->SetNDC();
  ra->SetTextFont(42);
  ra->SetTextColor(1);
  ra->SetTextSize(0.04);
  //  if (filename.Contains("ZCalib")) ra->DrawLatex(0.2,0.85,"#bf{#it{ATLAS Internal}}, #sqrt{s} = 13 TeV, 80.4 fb^{-1}");
  //ra->DrawLatex(0.2,0.85,"#bf{#it{ATLAS Internal}}, #sqrt{s} = 13 TeV, 139 fb^{-1}");
  ATLASLabel(0.2,0.88,"Internal");
  //ATLASLabel(0.2,0.2,"Work in progress");

  myText(       0.2,  0.82, 1, "#sqrt{s}= 13 TeV, 139 fb^{-1}");
  myText(       0.2, 0.40, 1, "Z(#rightarrow #font[12]{b#bar{b}})+jets calibration");
  myText(       0.2, 0.33, 1,"#epsilon_{X#rightarrow b#bar{b}}^{MC} = 60%, #mu_{post-tag}");  
  myText(       0.2, 0.26, 1,"p_{T}: "+_ptbin+" GeV");
 
  TPad *pad2 = new TPad("pad2", "pad2", 0, 0.0, 1, 0.35);
  pad2->cd();
  pad2->SetLeftMargin(0.15);
  pad2->SetTopMargin(0.0);
  pad2->SetBottomMargin(0.30);
  pad2->SetTicks();
  //pad2->SetGridY();
  RooHist *hpull = xframe->pullHist("data","model");
  hpull->SetFillColor(kGray + 1);
  hpull->SetLineColor(kGray + 1);
  hpull->SetMarkerColor(kGray + 1);
  hpull->SetMarkerSize(0);
  RooPlot *fpull = mass->frame();
  fpull->SetTitle("");
  fpull->addPlotable(hpull, "BX");
  //fpull->GetYaxis()->SetTitleOffset(0.5);
  //fpull->GetYaxis()->SetTitleSize(0.05);
  //fpull->GetYaxis()->SetLabelSize(0.05);
  //fpull->GetXaxis()->SetLabelSize(0.05);
  //fpull->GetYaxis()->SetLabelFont(43); // Absolute font size in pixel (precision 3)
  //fpull->GetYaxis()->SetLabelSize(25);
  //fpull->GetYaxis()->SetTitleOffset(1.5);
  //fpull->GetYaxis()->SetLabelOffset(0.01);
  //fpull->GetXaxis()->SetLabelFont(43); // Absolute font size in pixel (precision 3)
  //fpull->GetXaxis()->SetLabelSize(25);
  
  fpull->SetXTitle("");
  fpull->SetYTitle("Pull");
  fpull->SetMaximum(6);
  fpull->SetMinimum(-6);
  fpull->SetLineWidth(1.0);
  TAxis * _xAxis = fpull->GetXaxis();
  TAxis * _yAxis = fpull->GetYaxis();
  TLine * lc = new TLine(_xAxis->GetXmin(), 0, _xAxis->GetXmax(), 0);
  TLine * lu = new TLine(_xAxis->GetXmin(), 3, _xAxis->GetXmax(), 3);
  TLine * ld = new TLine(_xAxis->GetXmin(), -3, _xAxis->GetXmax(), -3);
  double old_size = _yAxis->GetLabelSize();
  _yAxis->SetNdivisions(5, 5, 1);
  _yAxis->SetLabelFont(43);
  _yAxis->SetLabelSize(25);
  _yAxis->SetLabelOffset(0.01);
  _yAxis->SetTitleFont(43);
  _yAxis->SetTitleSize(25);
  _yAxis->SetTitleOffset(2.0);
  _yAxis->CenterTitle(1);
  _yAxis->SetTitle("Pull");
  _xAxis->SetLabelFont(43);
  _xAxis->SetLabelSize(25);
  _xAxis->SetLabelOffset(0.008);
  _xAxis->SetTitleFont(45);
  _xAxis->SetTitleOffset(3.0);
  _xAxis->SetTitle("Large-#it{R} jet mass [GeV]"); 
  _xAxis->SetTitleSize(25);
 //_xAxis->SetLabelSize(0.15);
  //_xAxis->SetTitleSize(0);
  fpull->Draw();
  lc->SetLineColor(kGray + 2);
  lu->SetLineColor(kGray + 1);
  ld->SetLineColor(kGray + 1);
  lc->SetLineStyle(2);
  lu->SetLineStyle(2);
  ld->SetLineStyle(2);
  lc->Draw("SAME");
  lu->Draw("SAME");
  ld->Draw("SAME");
  double x,y;

  /* TH1F* pullhist = new TH1F("pullHist",20,-5.,5.);
  for(int i = 0;i<100;i++){
    hpull->GetPoint(10,x,y);
    pullhist->Fill(y);
    std::cout << y << std::endl;
    }*/
 
  
  TLatex * temOrmc = new TLatex(0.2,0.25,"1GeV/bin");
  temOrmc->SetNDC();
  temOrmc->SetTextFont(42);
  temOrmc->SetTextColor(1);
  temOrmc->SetTextSize(0.12);
  //  temOrmc->Draw();
  can->cd();
  pad1->Draw();
  pad2->Draw();
  can->SaveAs("res_"+_func+"_unbinned"+"_"+_Calib+"_"+_ptbin+".pdf");
  can->SaveAs("res_"+_func+"_unbinned"+"_"+_Calib+"_"+_ptbin+".png");
  can->SaveAs("res_"+_func+"_unbinned"+"_"+_Calib+"_"+_ptbin+".eps");

  //TCanvas* c = new TCanvas("can_corr"+filename,"can_corr"+filename,1200,1200) ;
  //c->cd();

  //gPad->SetLeftMargin(0.4) ; 
  //gPad->SetBottomMargin(0.4);
  //hcorr->GetYaxis()->SetTitleOffset(1.4) ; 
  //hcorr->SetStats(0);
  //hcorr->Draw("colz") ;
  //c->SaveAs(filename+"_"+_Calib+"_"+_ptbin+"correlation.pdf");
  return nll;
}

map<TString, double> calcChi2(TH1* hdata, TH1* hpdf, double blindMin, double blindMax){
  if(hdata->GetNbinsX() != hpdf->GetNbinsX()) {
    std::cout << "Number of bins do not match between data and pdf histograms used for chi2 calculation" << std::endl;
    exit(-1);
  }
  const int obsNBins = hdata->GetNbinsX();
  map<TString, double> result;
  bool goBlind = (blindMin < blindMax) && ( (blindMin > hdata->GetXaxis()->GetXmin()) || (blindMax < hdata->GetXaxis()->GetXmax()) );

  // ******************** Calculate chi2 ********************
  double chi2 = 0, content_data_chi2 = 0, content_pdf_chi2 = 0, error2_data_chi2 = 0, last_increment_chi2 = 0;
  int nbin_chi2 = 0, last_increment_bin_chi2 = 1;

  for( int ibin = 1 ; ibin <= obsNBins; ibin ++ ){
    if(goBlind && hdata->GetBinCenter(ibin) > blindMin && hdata->GetBinCenter(ibin) < blindMax ) continue;

    content_data_chi2 += hdata->GetBinContent(ibin);
    content_pdf_chi2 += hpdf->GetBinContent(ibin);
    error2_data_chi2 += pow(hdata->GetBinError(ibin), 2);

    if( content_data_chi2/sqrt(error2_data_chi2)<3;fabs(content_data_chi2) < 1e-6 ){ // Less than 3 sigma from 0 (if it is data it is 9 events)
      std::cout << "bin: " << ibin << std::endl;
      std::cout << "content_data_chi2: "<<content_data_chi2 <<std::endl;
      std::cout<< "error_data_chi2: "<<sqrt(error2_data_chi2) <<std::endl;
      std::cout<< "content_pdf_chi2: "<<content_pdf_chi2 <<std::endl;
      if(ibin<obsNBins) continue; // Not the last bin yet, continue aggregating
      else{// Reached last bin but still did not get 10 events, then merge back to last increment
        chi2 -= last_increment_chi2; // Subtract out last increment first
        content_data_chi2 = hdata->IntegralAndError(last_increment_bin_chi2, obsNBins, error2_data_chi2);
        error2_data_chi2 *= error2_data_chi2;
        content_pdf_chi2 = hpdf->Integral(last_increment_bin_chi2, obsNBins);
        chi2 += pow( (content_data_chi2-content_pdf_chi2), 2 )/error2_data_chi2;
        if(nbin_chi2==0) nbin_chi2++; // Corner case where the total number of data events is less than 10, in which case there should be one bin
  }
    }
    else{
      last_increment_chi2 = pow( (content_data_chi2-content_pdf_chi2), 2 )/error2_data_chi2;
      //std::cout << "bin: " << ibin << std::endl;
      //std::cout << "content_data_chi2: "<<content_data_chi2 <<std::endl;
      //std::cout<< "error_data_chi2: "<<sqrt(error2_data_chi2) <<std::endl;
      //std::cout<< "content_pdf_chi2: "<<content_pdf_chi2 <<std::endl;
      last_increment_bin_chi2 = ibin;
      chi2 += last_increment_chi2;
      nbin_chi2++;
      content_data_chi2 = 0;
      content_pdf_chi2 = 0;
      error2_data_chi2 = 0;
    }
  }

  // ******************** Calculate likelihood ********************
  double nll = 0, nllsat = 0, content_data_nll = 0, content_pdf_nll = 0, last_increment_nll = 0, last_increment_nllsat = 0;
  int nbin_nll = 0, last_increment_bin_nll = 1;

  for( int ibin = 1 ; ibin <= obsNBins; ibin ++ ){
    if(goBlind && hdata->GetBinCenter(ibin) > blindMin && hdata->GetBinCenter(ibin) < blindMax ) continue;

    content_data_nll += hdata->GetBinContent(ibin);
    content_pdf_nll += hpdf->GetBinContent(ibin);

    if( fabs(content_data_nll) < 2 ){ // reject empty bin
      if(ibin<obsNBins) continue; // Not the last bin yet, continue aggregating
      else{// Reached last bin but still did not get 10 events, then merge back to last increment
        nll -= last_increment_nll; // Subtract out last increment first
        nllsat -= last_increment_nllsat; // Subtract out last increment first
        content_data_nll = hdata->Integral(last_increment_bin_nll, obsNBins);
        content_pdf_nll = hpdf->Integral(last_increment_bin_nll, obsNBins);
        nll += -TMath::Log(TMath::Poisson(content_data_nll, content_pdf_nll));
        nllsat += -TMath::Log(TMath::Poisson(content_data_nll, content_data_nll)); // Saturated
        if(nbin_nll==0) nbin_nll++; // Corner case where the total number of data events is less than 10, in which case there should be one bin
  }
    }
    else{
      last_increment_nll = -TMath::Log(TMath::Poisson(content_data_nll, content_pdf_nll));
      last_increment_nllsat = -TMath::Log(TMath::Poisson(content_data_nll, content_data_nll));
      nll += last_increment_nll;
      nllsat += last_increment_nllsat;
      last_increment_bin_nll = ibin;

      nbin_nll++;

      content_data_nll = 0;
      content_pdf_nll = 0;
    }

  }

  result["chi2"] = chi2;
  result["nbinchi2"] = nbin_chi2;
  result["nll"] = nll;
  result["nllsat"] = nllsat;
  result["nbinnll"] = nbin_nll;

  return result;
}
