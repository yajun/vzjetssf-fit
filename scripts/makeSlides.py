#!/bin/python3
import glob
import re
import os

cats = ["zbb","ttbarCatTop","ttbarCatWOther"]
pts = ["450-500","500-600","600-700","700-1000"]

with open('/sps/atlas/y/yahe/Vjets-fit/templateFit/slides/tex/Zjslide.tex', 'w') as tex:
    for cat in cats:
        for pt in pts:
            print >>tex,'\\begin{frame}'
            print >>tex,'   \includegraphics[width=0.9\\textwidth]{/sps/atlas/y/yahe/Vjets-fit/templateFit/output/ZCalib/'+pt+"/plot/"+cat+'_nominal.pdf}\\\\'
            print >>tex, ' \end{frame}'

cats = ["zqq","wqq","ttbarCatTop","ttbarCatW","ttbarCatOther"]
pts = ["500-600","600-700","700-800","800-1000","1000-1500"]
with open('/sps/atlas/y/yahe/Vjets-fit/templateFit/slides/tex/Vjslide.tex', 'w') as tex:
    for cat in cats:
        for pt in pts:
            print >>tex,'\\begin{frame}'
            print >>tex,'   \includegraphics[width=0.9\\textwidth]{/sps/atlas/y/yahe/Vjets-fit/templateFit/output/VCalib/'+pt+"/plot/"+cat+'_nominal.pdf}\\\\'

            print >>tex, ' \end{frame}'
