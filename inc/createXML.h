#ifndef CREATEXML_HEADER
#define CREATEXML_HEADER

#include "TROOT.h"
#include "TXMLEngine.h"

#include <iostream>
#include <fstream>
#include <iostream>
#include <string>
#include <map>

using namespace std;

class createXML : public TObject{
 public:

  static void createDSCBPdfXML(TString systematic,TString ptbin,TString whichprocess,map<TString, double> DSCBparas);
  static void createQCDPdfXML(TString ptbin,TString whichorder);
  static void createCategoryXML(map<TString,double> yields, TString systematic, TString ptbin, TString bkgorder, TString xmlfilename );
  static void createTopXML(TString systematic, TString ptbin, TString bkgorder, TString xmlfilename );

  static void createAllSystDSCBPdfXML(TString ptbin,TString whichprocess, map<TString, double> DSCBparas);
  static void createAllSystCategoryXML(map<TString, double> yields,TString ptbin, TString bkgorder, TString xmlfilename);
  static void createAllSystTopXML(TString ptbin, TString bkgorder, TString xmlfilename);
  
  ClassDef(createXML,1);
};

#endif
