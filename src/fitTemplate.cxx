#include "fitTemplate.h"

#include "TROOT.h"
#include "TSystem.h"
#include "TCanvas.h"
#include "TAxis.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TH2.h"
#include "TFile.h"

#include "caliConfig.h"
#include "getData.h"

ClassImp(fitTemplate)

// Default values for the minimizer
int fitTemplate::_minimizerStrategy=1;
string fitTemplate::_minimizerAlgo="Minuit2";
double fitTemplate::_minimizerTolerance=1e-3;
bool fitTemplate::_nllOffset=true;
int fitTemplate::_printLevel=-1;

int fitTemplate::doFit(TString filename, double ptmin, double ptmax, TString whichprocess, TString ptbin){
  double mmin = 50.0;
  double mmax = 150.0;
  if(whichprocess.Contains("ttbarCatTop")) mmax = 250.0;
  if(whichprocess.Contains("ttbarCatW")) mmax = 150.0;
  if(whichprocess.Contains("ttbarCatOther")||whichprocess.Contains("ttbarCatWOther")) mmax = 200.0;
  auto*  mass = new RooRealVar(caliConfig::_Category+"_mass",caliConfig::_Category+"_mass",mmin,mmax) ;
  //auto* weight = new RooRealVar("weight","weight",0.,100000) ;
  double yields = 0;
  double binWidth = 2.5;
  if(caliConfig::_Calib.Contains("VCalib")) binWidth = 2.5;
  int nbins = (mmax-mmin)/binWidth;
  std::cout << "nbins: " << nbins << std::endl;
  double mu0 = 91.0;
  if(whichprocess.Contains("ttbarCatTop")) mu0 = 173.21;
  if(whichprocess.Contains("wqq") || whichprocess.Contains("ttbarCatW")) mu0 = 80.385;
  if(whichprocess.Contains("ttbarCatOther")||whichprocess.Contains("ttbarCatWOther")) mu0 = 125.0;

  double muhi = mu0+20.0;
  double mulo =mu0-20.0;
  if(whichprocess.Contains("wqq")){
    muhi = mu0+10.0;
    mulo =mu0-10.0;
  }

  RooDataSet* wdata = getData::importdata(filename, ptmin, ptmax,whichprocess);
  mass->setBins(nbins);
  yields = wdata->sumEntries();
  std::cout << yields << std::endl;
  RooDataHist* binnedData = wdata->binnedClone();
  wdata->Print("v");
  binnedData->Print("v");

  RooRealVar*  Yields = new RooRealVar("Yield","Yield",yields) ;

  RooRealVar *muCB = new RooRealVar("mu", "mu", mu0, mulo, muhi);
  RooRealVar *sigmaCB = new RooRealVar("sigma","sigma",7.6792,0.1, 40.);
  RooRealVar *alphaCBLo = new RooRealVar("alphaCBLo","alphaCBLo",1.2766,0,1000);
  RooRealVar *nCBLo = new RooRealVar("nCBLo","nCBLo",7.13,0,1000);
  RooRealVar *alphaCBHi = new RooRealVar("alphaCBHi","alphaCBHi",1.86,0,1000);
  RooRealVar *nCBHi = new RooRealVar("nCBHi","nCBHi",0.42,0,1000);

  RooTwoSidedCBShape Pdf("DSCBPdf","DSCBPdf",*mass,*muCB, *sigmaCB, *alphaCBLo, *nCBLo, *alphaCBHi, *nCBHi);

  RooFitResult* ml_wgt = Pdf.fitTo(*wdata,Offset(_nllOffset),Minimizer("Minuit2","minimize"),Strategy(_minimizerStrategy),Save(true), SumW2Error(true), PrintLevel(_printLevel));

  //Save and visulize fit results
  TString DirToSaveFitresults = caliConfig::_FitOutputDir + caliConfig::_Calib + "/" + ptbin + "/fitresults/";
  TString DirToSavePlot = caliConfig::_FitOutputDir + caliConfig::_Calib + "/" + ptbin + "/plot/";
  gSystem->Exec("mkdir -p " + DirToSaveFitresults);
  gSystem->Exec("mkdir -p " + DirToSavePlot);
  TFile f(DirToSaveFitresults + "/"+whichprocess+".root","RECREATE") ;
  ml_wgt->Write("fitresults") ;
  Yields->Write("Yields") ;
  f.Close() ;
  std::cout << "Saved roofit results to : " << DirToSaveFitresults <<  "/" << whichprocess << ".root" << std::endl; 

  RooPlot* frame = mass->frame() ;
  wdata->plotOn(frame,Name("Prefit"),DataError(RooAbsData::SumW2)) ;
  Pdf.plotOn(frame,Name("ML"),LineStyle(kDashed),LineColor(kRed)) ;
  
  TCanvas* c = new TCanvas(whichprocess,"",1000,1000) ;
  c->Divide(2,2);
  c->cd(1);
  gPad->SetLeftMargin(0.15) ;
  frame->SetXTitle("Mass / 2.5GeV");
  frame->SetYTitle("Events");
  frame->SetMaximum(frame->GetMaximum()*1.5);
  frame->GetYaxis()->SetTitleOffset(1.8) ; frame->Draw() ;

  double fsp = 0.40;
  if(whichprocess.Contains("wqq")) fsp = 0.30;
  TLatex * fstatus = new TLatex(fsp,0.15," Fit status: " + TString::Format("%1.1d",ml_wgt->status()));
  fstatus->SetNDC();
  fstatus->SetTextFont(42);
  fstatus->SetTextColor(1);
  fstatus->SetTextSize(0.03);
  //fstatus->Draw();

  double lp = 0.50;
  if(whichprocess.Contains("ttbarCatTop")) lp = 0.17;
  double rp = lp+0.1;
  if(whichprocess.Contains("ttbarCatTop")) rp = lp+0.05;
  
  TLatex * yld = new TLatex(lp+0.1,0.3," MC Yields: " + TString::Format("%.1f",Yields->getVal()));
  yld->SetNDC();
  yld->SetTextFont(42);
  yld->SetTextColor(1);
  yld->SetTextSize(0.03);
  yld->Draw();

  TLatex *paras = new TLatex();
  paras->SetNDC();
  paras->SetTextFont(42);
  paras->SetTextColor(1);
  paras->SetTextSize(0.03);
  paras->DrawLatex(lp,0.70,Form("#chi^{2} : %1.2f; Status: %1.1d",frame->chiSquare(6),ml_wgt->status()));
  if(whichprocess.Contains("zbb")) paras->DrawLatex(lp,0.65,Form("#bf{Zbb : %1.0f #leq  p_{T} < %1.0f GeV}",ptmin,ptmax));
  if(whichprocess.Contains("zqq")) paras->DrawLatex(lp,0.65,Form("#bf{Zqq : %1.0f #leq  p_{T} < %1.0f GeV}",ptmin,ptmax));
  if(whichprocess.Contains("wqq")) paras->DrawLatex(lp,0.65,Form("#bf{Wqq : %1.0f #leq  p_{T} < %1.0f GeV}",ptmin,ptmax));
  if(whichprocess.Contains("ttbarCatTop")) paras->DrawLatex(lp,0.65,Form("#bf{t#bar{t} CatTop: %1.0f #leq  p_{T} < %1.0f GeV}",ptmin,ptmax));
  if(whichprocess.Contains("ttbarCatW") && !whichprocess.Contains("ttbarCatWOther")) paras->DrawLatex(lp,0.65,Form("#bf{t#bar{t} CatW: %1.0f #leq  p_{T} < %1.0f GeV}",ptmin,ptmax));
  if(whichprocess.Contains("ttbarCatOther")) paras->DrawLatex(lp,0.65,Form("#bf{t#bar{t} CatOther: %1.0f #leq  p_{T} < %1.0f GeV}",ptmin,ptmax));
  if(whichprocess.Contains("ttbarCatWOther")) paras->DrawLatex(lp,0.65,Form("#bf{t#bar{t} CatW&Other: %1.0f #leq  p_{T} < %1.0f GeV}",ptmin,ptmax));
  paras->DrawLatex(lp+0.1,0.60,Form("#mu:  %1.2f #pm %1.2f",muCB->getVal(),muCB->getError()));
  paras->DrawLatex(lp+0.1,0.55,Form("#sigma:  %1.2f #pm %1.2f",sigmaCB->getVal(),sigmaCB->getError()));
  paras->DrawLatex(lp+0.1,0.50,Form("#alpha_{L} :  %1.2f #pm %1.2f",alphaCBLo->getVal(),alphaCBLo->getError()));
  paras->DrawLatex(lp+0.1,0.45,Form("n_{L} :  %1.2f #pm %1.2f",nCBLo->getVal(),nCBLo->getError()));
  paras->DrawLatex(lp+0.1,0.40,Form("#alpha_{H} :  %1.2f #pm %1.2f",alphaCBHi->getVal(),alphaCBHi->getError()));
  paras->DrawLatex(lp+0.1,0.35,Form("n_{H} :  %1.2f #pm %1.2f",nCBHi->getVal(),nCBHi->getError()));

  TLatex *test = new TLatex();
  test->SetNDC();
  test->SetTextColor(kBlack);
  test->SetTextFont(42);
  test->SetTextSize(0.030);
  if(caliConfig::_Calib.Contains("ZCalib")){
    test->DrawLatex(0.17,0.85,"#bf{#it{ATLAS Internal}},80.4 fb^{-1}");
    test->DrawLatex(0.17,0.80,"Xbb Tagger, 60%WP");
    test->DrawLatex(0.17,0.75,"Z+jets calibration");
  }if(caliConfig::_Calib.Contains("VCalib")){
    test->DrawLatex(0.17,0.85,"#bf{#it{ATLAS Internal}},138.9 fb^{-1}");
    test->DrawLatex(0.17,0.80,"Smooth W/Z Tagger, 50%WP");
    test->DrawLatex(0.17,0.75,"V+jets calibration");
  }
  if(caliConfig::_Category.Contains("lj1")) test->DrawLatex(0.17,0.70,"leading large-R jet");
  if(caliConfig::_Category.Contains("lj2")) test->DrawLatex(0.17,0.70,"subleading large-R jet");

  TLegend *leg1 = new TLegend(0.55,0.75,0.85,0.85);
  leg1->SetTextFont(42);
  leg1->SetTextSize(0.04);
  leg1->SetBorderSize(0);
  leg1->SetMargin(0.3);
  leg1->SetFillColor(kWhite);
  leg1->SetLineColor(kWhite);
  leg1->AddEntry("Prefit","Prefit", "LP");
  leg1->AddEntry("ML","unbinned ML","L");
  leg1->Draw();
  std::cout << "============================Fit Results===========================" << std::endl;
  std::cout << " Chi2(" << binWidth <<"GeV per bin): " << frame->chiSquare(6) << std::endl; 
  std::cout << "============================ML Fit Results===========================" << std::endl;
  ml_wgt->Print();
  std::cout << "============================Yields===========================" << std::endl;
  std::cout << "yields: " << yields << std::endl;
  std::cout << "============================Saving Fit Results===========================" << std::endl;
  c->cd(2);
  TH2 *hcorr = ml_wgt->correlationHist();
  gPad->SetLeftMargin(0.15);
  gPad->SetRightMargin(0.15);
  hcorr->SetStats(0);
  hcorr->GetYaxis()->SetTitleOffset(1.4);
  hcorr->Draw("colz");
  c->cd(3);
  RooHist *hresid = frame->residHist();
  RooPlot *frame2 = mass->frame(Title("Residual Distribution"));
  frame2->addPlotable(hresid, "P");
  gPad->SetLeftMargin(0.15);
  frame2->GetYaxis()->SetTitleOffset(1.6);
  frame2->Draw();

  c->cd(4);
  RooHist *hpull = frame->pullHist();
  RooPlot *frame3 = mass->frame(Title("Pull Distribution"));
  frame3->addPlotable(hpull, "P");
  gPad->SetLeftMargin(0.15);
  frame3->GetYaxis()->SetTitleOffset(1.6);
  frame3->Draw();

  c->cd();
  c->SaveAs(DirToSavePlot+"/"+whichprocess+".pdf","RECREATE");

  return ml_wgt->status();
}

map<TString, double> fitTemplate::getDSCBParas(TString pathtofitresult) {
  TFile *file = new TFile(pathtofitresult);
  RooFitResult *result = (RooFitResult*)file->Get("fitresults");
  RooRealVar*  Yields = (RooRealVar*)file->Get("Yields");
  RooRealVar *muCB = (RooRealVar*) result->floatParsFinal().find("mu");
  RooRealVar *sigmaCB = (RooRealVar*) result->floatParsFinal().find("sigma");
  RooRealVar *alphaCBLo = (RooRealVar*) result->floatParsFinal().find("alphaCBLo");
  RooRealVar *nCBLo = (RooRealVar*) result->floatParsFinal().find("nCBLo");
  RooRealVar *alphaCBHi = (RooRealVar*) result->floatParsFinal().find("alphaCBHi");
  RooRealVar *nCBHi = (RooRealVar*) result->floatParsFinal().find("nCBHi");
  Yields->setConstant(kTRUE);
  muCB->setConstant(kTRUE);
  sigmaCB->setConstant(kTRUE);
  alphaCBLo->setConstant(kTRUE);
  nCBLo->setConstant(kTRUE);
  alphaCBHi->setConstant(kTRUE);
  nCBHi->setConstant(kTRUE);
  map<TString, double> results;
  results["Yields"] = Yields->getVal();
  results["mu"] = muCB->getVal();
  results["sigma"] = sigmaCB->getVal();
  results["alphaLo"] = alphaCBLo->getVal();
  results["nLo"] = nCBLo->getVal();
  results["alphaHi"] = alphaCBHi->getVal();
  results["nHi"] = nCBHi->getVal();
  file->Close();
  return results;
}

map<TString, map<TString, map<TString, double>>> fitTemplate::getSystsMap(TString whichprocess, TString pathtofitresult) { //(syst, DSCB paras)
  map<TString, map<TString, map<TString, double>>> results;
  //std::vector<TString> SystsList = getData::getSystsList();
  map<TString,double> res_nominal = getDSCBParas(pathtofitresult+"/"+whichprocess+"_nominal.root");
  results["nominal"]["nominal"] = res_nominal;
  map<TString,double> res_sh228lund = getDSCBParas(pathtofitresult+"/"+whichprocess+"_sh228lund.root");
  results["sh228lund"]["sh228lund"] = res_nominal;
  for (uint i=2;i<caliConfig::_Systematics.size();++i) {
    TString syst = caliConfig::_Systematics[i];
    map<TString,double> res_up = getDSCBParas(pathtofitresult+"/"+whichprocess+"_"+syst+"__1up.root");
    map<TString,double> res_down = getDSCBParas(pathtofitresult+"/"+whichprocess+"_"+syst+"__1down.root");
    results[syst]["up"] = res_up;
    results[syst]["down"]= res_down;
  }
  return results;
}

