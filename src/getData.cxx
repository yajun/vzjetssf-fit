#include "getData.h"

ClassImp(getData)

//TString getData::_Calib = "ZCalib";
//TString getData::_InputDir = "/sps/atlas/y/yahe/hadd-zbb/";
//TString getData::_TreeName = "Ztag";

RooDataSet* getData::importdata(TString filename, double ptmin, double ptmax, TString whichprocess) {
  double mmin = 50.0;
  double mmax = 150.0;
  if(whichprocess.Contains("ttbarCatTop")) mmax = 250.0;
  if(whichprocess.Contains("ttbarCatW") && !whichprocess.Contains("ttbarCatWOther")) mmax = 150.0;
  if(whichprocess.Contains("ttbarCatWOther")|| whichprocess.Contains("ttbarCatOther")) mmax = 200.0;
  TFile * tf = new TFile(caliConfig::_InputDir+filename,"READ");
  TTree *tt = (TTree*) tf->Get(caliConfig::_TreeName);
  TFile * temp = new TFile("temp.root","RECREATE");
  auto tn = tt->CloneTree(0);
  const auto nentries = tt->GetEntries();
  if(caliConfig::_Calib.Contains("ZCalib")){
    float pt;
    int truthlabel;
    int blabel1;
    int blabel2;
    int blabel3;
    int nVRjets;
    int isxbb;
    tt->SetBranchAddress(caliConfig::_Category+"_pt", &pt);
    tt->SetBranchAddress(caliConfig::_Category+"_truthlabel", &truthlabel);
    tt->SetBranchAddress(caliConfig::_Category+"_VRjet1_id", &blabel1);
    tt->SetBranchAddress(caliConfig::_Category+"_VRjet2_id", &blabel2);
    tt->SetBranchAddress(caliConfig::_Category+"_VRjet3_id", &blabel3);
    tt->SetBranchAddress(caliConfig::_Category+"_isXbb_60",&isxbb);
    tt->SetBranchAddress(caliConfig::_Category+"_nVRjets",&nVRjets);
    for (auto i : ROOT::TSeqI(nentries)) {
      tt->GetEntry(i);
      if (whichprocess.Contains("zbb")){
	if (pt > ptmin && pt<ptmax && nVRjets>=2 && (truthlabel == 3 || truthlabel == 6) && (blabel1+blabel2==10 || blabel2+blabel3==10 || blabel1+blabel3==10) && isxbb == 1) tn->Fill();
      }else if (whichprocess.Contains("zqq")){
	//if (pt > ptmin && pt<ptmax && nVRjets>=2 && (truthlabel == 3 || truthlabel == 6) && (blabel1 == 4 || blabel2 == 4 || blabel3 == 4)) tn->Fill(); //at least 1c
	if (pt > ptmin && pt<ptmax && nVRjets>=2 && (truthlabel == 3 || truthlabel == 6) && isxbb == 1) tn->Fill();
      }else if (whichprocess.Contains("wqq")){
	//if (pt > ptmin && pt<ptmax && nVRjets>=2 && (truthlabel == 2 || truthlabel == 6) && (blabel1 == 4 || blabel2 == 4 || blabel3 == 4)) tn->Fill(); //at least 1c
	if (pt > ptmin && pt<ptmax && nVRjets>=2 && (truthlabel == 2 || truthlabel == 6) && isxbb == 1) tn->Fill();
      }else if (whichprocess.Contains("ttbarCatTop")){
	if (pt > ptmin && pt<ptmax && nVRjets>=2 && (truthlabel == 1) && isxbb == 1) tn->Fill(); //tqqb
      }else if (whichprocess.Contains("ttbarCatW") && !whichprocess.Contains("ttbarCatWOther") ){
	if (pt > ptmin && pt<ptmax && nVRjets>=2 && (truthlabel == 4 ) && isxbb == 1) tn->Fill(); //wqq from top
      }else if (whichprocess.Contains("ttbarCatOther")){
	if (pt > ptmin && pt<ptmax && nVRjets>=2 && (truthlabel == 5 ) && isxbb == 1) tn->Fill();//other from top
      }else if (whichprocess.Contains("ttbarCatWOther")){
	if (pt > ptmin && pt<ptmax && nVRjets>=2 && (truthlabel == 5 || truthlabel == 4) && isxbb == 1) tn->Fill(); //W&Other from Top
      }
    }
 
  } else if(caliConfig::_Calib.Contains("VCalib")){
    float pt;
    int truthlabel;
    int is_Wtagged;
    int is_passWD2;
    int is_Ztagged;
    int is_passZD2;
    float ptAsy;
    float dy;
    tt->SetBranchAddress(caliConfig::_Category+"_pt", &pt);
    tt->SetBranchAddress(caliConfig::_Category+"_truthlabel", &truthlabel);
    tt->SetBranchAddress(caliConfig::_Category+"_isWTagged_50", &is_Wtagged);
    tt->SetBranchAddress(caliConfig::_Category+"_isZTagged_50", &is_Ztagged);
    if(caliConfig::_Category.Contains("lj1")){
      tt->SetBranchAddress("lj2_passWD2_50", &is_passWD2);
      tt->SetBranchAddress("lj2_passZD2_50", &is_passZD2);
    }else{
      tt->SetBranchAddress("lj1_passWD2_50", &is_passWD2);
      tt->SetBranchAddress("lj1_passZD2_50", &is_passZD2);
    }
    tt->SetBranchAddress("ptAsy", &ptAsy);
    tt->SetBranchAddress("dy", &dy);
    for (auto i : ROOT::TSeqI(nentries)) {
      tt->GetEntry(i);
      if (whichprocess.Contains("zbb")){
        if (pt > ptmin && pt<ptmax && ptAsy<0.15 && dy<1.2 && (truthlabel == 3 || truthlabel == 6) && ((is_Wtagged == 1 && is_passWD2 == 0) || (is_Ztagged == 1 && is_passZD2 == 0))) tn->Fill();
      }else if (whichprocess.Contains("zqq")){
        if (pt > ptmin && pt<ptmax && ptAsy<0.15 && dy<1.2 && (truthlabel == 3 || truthlabel == 6) && ((is_Wtagged == 1 && is_passWD2 == 0) || (is_Ztagged == 1 && is_passZD2 == 0))) tn->Fill(); 
      }else if (whichprocess.Contains("wqq")){
        if (pt > ptmin && pt<ptmax && ptAsy<0.15 && dy<1.2 && (truthlabel == 2 || truthlabel == 6) && ((is_Wtagged == 1 && is_passWD2 == 0) || (is_Ztagged == 1 && is_passZD2 == 0))) tn->Fill();
      }else if (whichprocess.Contains("ttbarCatTop")){
        if (pt > ptmin && pt<ptmax && ptAsy<0.15 && dy<1.2 && (truthlabel == 1) && ((is_Wtagged == 1 && is_passWD2 == 0) || (is_Ztagged == 1 && is_passZD2 == 0))) tn->Fill(); //tqqb
      }else if (whichprocess.Contains("ttbarCatW")){
        if (pt > ptmin && pt<ptmax && ptAsy<0.15 && dy<1.2 && (truthlabel == 4 ) && ((is_Wtagged == 1 && is_passWD2 == 0) || (is_Ztagged == 1 && is_passZD2 == 0))) tn->Fill(); //wqq from top
      }else if (whichprocess.Contains("ttbarCatOther")){
        if (pt > ptmin && pt<ptmax && ptAsy<0.15 && dy<1.2 && (truthlabel == 5 ) && ((is_Wtagged == 1 && is_passWD2 == 0) || (is_Ztagged == 1 && is_passZD2 == 0))) tn->Fill();//other from top
      }
    }
  }
  RooRealVar*  mass = new RooRealVar(caliConfig::_Category+"_mass",caliConfig::_Category+"_mass",mmin,mmax) ;
  auto * weight = new RooRealVar("weight","weight",-1.0e+08,1.0e+08) ;
  auto * ds = new RooDataSet("ds","ds",tn,RooArgSet(*mass,*weight),0,"weight") ;
  tf->Close();
  temp->Delete();
  temp->Close();
  return ds;
}

double getData::getyield(TString filename, double ptmin, double ptmax, TString whichprocess){
  double mmin = 50.0;
  double mmax = 150.0;
  TFile * tf = new TFile(caliConfig::_InputDir+filename,"READ");
  TTree *tt = (TTree*) tf->Get(caliConfig::_TreeName);
  const auto nentries = tt->GetEntries();
  double sumofweight = 0.;
  if(caliConfig::_Calib.Contains("ZCalib")){
    float m;
    float pt;
    int truthlabel;
    int blabel1;
    int blabel2;
    int blabel3;
    int nVRjets;
    int isxbb;
    float wgt;
    tt->SetBranchAddress(caliConfig::_Category+"_mass", &m);
    tt->SetBranchAddress(caliConfig::_Category+"_pt", &pt);
    tt->SetBranchAddress(caliConfig::_Category+"_truthlabel", &truthlabel);
    tt->SetBranchAddress(caliConfig::_Category+"_VRjet1_id", &blabel1);
    tt->SetBranchAddress(caliConfig::_Category+"_VRjet2_id", &blabel2);
    tt->SetBranchAddress(caliConfig::_Category+"_VRjet3_id", &blabel3);
    tt->SetBranchAddress(caliConfig::_Category+"_isXbb_60",&isxbb);
    tt->SetBranchAddress(caliConfig::_Category+"_nVRjets",&nVRjets);
    tt->SetBranchAddress("weight",&wgt);
    for (auto i : ROOT::TSeqI(nentries)) {
      tt->GetEntry(i);
      if (m>mmin && m<mmax){
	if (whichprocess.Contains("zbb")){
	  if (pt > ptmin && pt<ptmax && nVRjets>=2 && (truthlabel == 3 || truthlabel == 6) && (blabel1+blabel2==10 || blabel2+blabel3==10 || blabel1+blabel3==10) && isxbb == 1) sumofweight= sumofweight+wgt;
	} else if (whichprocess.Contains("zqq")){
	if (pt > ptmin && pt<ptmax && nVRjets>=2 && (truthlabel == 3 || truthlabel == 6) && isxbb == 1) sumofweight = sumofweight + wgt; //zqq
	}else if (whichprocess.Contains("wqq")){
	  if (pt > ptmin && pt<ptmax && nVRjets>=2 && (truthlabel == 2 || truthlabel == 6) && isxbb == 1) sumofweight = sumofweight + wgt; //wqq
	}else if (whichprocess.Contains("ttbarCatTop")){
	  if (pt > ptmin && pt<ptmax && nVRjets>=2 && (truthlabel == 1) && isxbb == 1) sumofweight = sumofweight + wgt; //tqqb
	}else if (whichprocess.Contains("ttbarCatW")&& !whichprocess.Contains("ttbarCatWOther")){
	  if (pt > ptmin && pt<ptmax && nVRjets>=2 && (truthlabel == 4 ) && isxbb == 1) sumofweight = sumofweight + wgt; //wqq from top
	}else if (whichprocess.Contains("ttbarCatOther")){
	  if (pt > ptmin && pt<ptmax && nVRjets>=2 && (truthlabel == 5 ) && isxbb == 1) sumofweight = sumofweight + wgt;//other from top 
	}
      }
    }
  }else if(caliConfig::_Calib.Contains("VCalib")){
    float pt;
    int truthlabel;
    int is_Wtagged;
    int is_passWD2;
    int is_Ztagged;
    int is_passZD2;
    float ptAsy;
    float dy;
    float wgt;
    TFile * temp = new TFile("temp.root","RECREATE");
    tt->SetBranchAddress(caliConfig::_Category+"_pt", &pt);
    tt->SetBranchAddress(caliConfig::_Category+"_truthlabel", &truthlabel);
    tt->SetBranchAddress(caliConfig::_Category+"_isWTagged_50", &is_Wtagged);
    tt->SetBranchAddress(caliConfig::_Category+"_isZTagged_50", &is_Ztagged);
    if(caliConfig::_Category.Contains("lj1")){
      tt->SetBranchAddress("lj2_passWD2_50", &is_passWD2);
      tt->SetBranchAddress("lj2_passZD2_50", &is_passZD2);
    }else{
      tt->SetBranchAddress("lj1_passWD2_50", &is_passWD2);
      tt->SetBranchAddress("lj1_passZD2_50", &is_passZD2);
    }    
    tt->SetBranchAddress("ptAsy", &ptAsy);
    tt->SetBranchAddress("dy", &dy);
    tt->SetBranchAddress("weight",&wgt);
    auto tn = tt->CloneTree(0);
    for (auto i : ROOT::TSeqI(nentries)) {
      tt->GetEntry(i);
      if (whichprocess.Contains("zbb")){
        if (pt > ptmin && pt<ptmax && ptAsy<0.15 && dy<1.2 && (truthlabel == 3 || truthlabel == 6) && ((is_Wtagged == 1 && is_passWD2 == 0) || (is_Ztagged == 1 && is_passZD2 == 0))) sumofweight= sumofweight+wgt;
      }else if (whichprocess.Contains("zqq")){
        if (pt > ptmin && pt<ptmax && ptAsy<0.15 && dy<1.2 && (truthlabel == 3 || truthlabel == 6) && ((is_Wtagged == 1 && is_passWD2 == 0) || (is_Ztagged == 1 && is_passZD2 == 0))) sumofweight= sumofweight+wgt;
      }else if (whichprocess.Contains("wqq")){
        if (pt > ptmin && pt<ptmax && ptAsy<0.15 && dy<1.2 && (truthlabel == 2 || truthlabel == 6) && ((is_Wtagged == 1 && is_passWD2 == 0) || (is_Ztagged == 1 && is_passZD2 == 0))) sumofweight= sumofweight+wgt;
      }else if (whichprocess.Contains("ttbarCatTop")){
        if (pt > ptmin && pt<ptmax && ptAsy<0.15 && dy<1.2 && (truthlabel == 1) && ((is_Wtagged == 1 && is_passWD2 == 0) || (is_Ztagged == 1 && is_passZD2 == 0))) sumofweight= sumofweight+wgt; //tqqb
      }else if (whichprocess.Contains("ttbarCatW")){
        if (pt > ptmin && pt<ptmax && ptAsy<0.15 && dy<1.2 && (truthlabel == 4 ) && ((is_Wtagged == 1 && is_passWD2 == 0) || (is_Ztagged == 1 && is_passZD2 == 0))) sumofweight= sumofweight+wgt; //wqq from top
      }else if (whichprocess.Contains("ttbarCatOther")){
        if (pt > ptmin && pt<ptmax && ptAsy<0.15 && dy<1.2 && (truthlabel == 5 ) && ((is_Wtagged == 1 && is_passWD2 == 0) || (is_Ztagged == 1 && is_passZD2 == 0))) sumofweight= sumofweight+wgt;//other from top
      }
    }
  }
  tf->Close();
  return sumofweight;
}

map<TString, double> getData::getyields(double ptmin, double ptmax){
  map<TString, double> result;
  if(caliConfig::_Calib.Contains("ZCalib")){
    result["zbb"] = getyield("zbb-nominal.root",ptmin,ptmax,"zbb");
    result["zqq"] = getyield("zqq-nominal.root",ptmin,ptmax,"zqq");
    result["wqq"] = getyield("wqq-nominal.root",ptmin,ptmax,"wqq");
    result["ttbarCatTop"] = getyield("ttbar-nominal.root",ptmin,ptmax,"ttbarCatTop");
    result["ttbarCatW"] = getyield("ttbar-nominal.root",ptmin,ptmax,"ttbarCatW");
    result["ttbarCatOther"] = getyield("ttbar-nominal.root",ptmin,ptmax,"ttbarCatOther");
    result["ttbarCatWOther"] = result["ttbarCatW"] + result["ttbarCatOther"];
  }else if(caliConfig::_Calib.Contains("VCalib")){
    result["zqq"] = getyield("zqq-nominal.root",ptmin,ptmax,"zqq");
    result["wqq"] = getyield("wqq-nominal.root",ptmin,ptmax,"wqq");
    result["ttbarCatTop"] = getyield("ttbar-nominal.root",ptmin,ptmax,"ttbarCatTop");
    result["ttbarCatW"] = getyield("ttbar-nominal.root",ptmin,ptmax,"ttbarCatW");
    result["ttbarCatOther"] = getyield("ttbar-nominal.root",ptmin,ptmax,"ttbarCatOther");
  }
  return result;
}
