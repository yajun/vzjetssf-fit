#include <TGraph.h>
#include <TGraphErrors.h>
#include <TLatex.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TFile.h>
#include <TString.h>
#include <TProfile.h>
#include <TH1D.h>
#include <TF1.h>
#include <TF2.h>
#include <TLine.h>
#include <vector>
#include <iostream>
#include <fstream>

void plotMagnitudeOfSystMass(){

  gROOT->SetBatch(kTRUE);
  TString _ptbin ="500-600";
  TString _Calib = "VCalib";
  TString filename = "../xml/"+_Calib+"/"+_ptbin+"/syst_mag";
  TFile *f = new TFile(filename+".root");
  TString _processes[5] = {"wqq","zqq","ttbarCatTop","ttbarCatW","ttbarCatOther"};
  for(int ip = 0; ip<5; ip++){
    TString _process = _processes[ip];
    TH1F* h_mu_up = (TH1F*) f->Get(_process+"_mu_up");
    TH1F* h_mu_down = (TH1F*) f->Get(_process+"_mu_down");
    h_mu_down->Scale(-1);
    TCanvas* c = new TCanvas("hist","hist",1200,800);
    c->SetTopMargin(0.05);
    c->SetBottomMargin(0.5);
    h_mu_up->SetYTitle("(syst.-nom.)/nom.");
    h_mu_up->GetYaxis()->SetTitleOffset(1.0) ;
    h_mu_up->SetMaximum(0.2);
    h_mu_up->SetMinimum(-0.2);
    h_mu_up->SetLineWidth(2);
    h_mu_up->SetLineColor(kRed);
    h_mu_up->LabelsOption("v");
    h_mu_up->SetTitle("");
    h_mu_up->GetXaxis()->SetLabelFont(43);
    h_mu_up->GetXaxis()->SetLabelSize(15);
    h_mu_up->SetStats(0);
    h_mu_up->Draw("hist");

    h_mu_down->SetLineWidth(2);
    h_mu_down->SetLineColor(kBlue);
    h_mu_down->LabelsOption("v");
    h_mu_down->SetTitle("");
    h_mu_down->GetXaxis()->SetLabelFont(43);
    h_mu_down->GetXaxis()->SetLabelSize(15);
    h_mu_down->SetStats(0);
    h_mu_down->Draw("hist same");

    TLegend *leg1 = new TLegend(0.40,0.75,0.65,0.85);
    leg1->SetTextFont(42);
    leg1->SetTextSize(0.035);
    leg1->SetBorderSize(0);
    leg1->SetMargin(0.3);
    leg1->SetFillColor(kWhite);
    leg1->SetLineColor(kWhite);
    leg1->AddEntry(h_mu_up,"DSCBmass_"+_process+"_up", "L");
    leg1->AddEntry(h_mu_down,"DSCBmass_"+_process+"_down","L");
    leg1->Draw();

    TLatex * ra = new TLatex();
    ra->SetNDC();
    ra->SetTextFont(42);
    ra->SetTextColor(1);
    ra->SetTextSize(0.035);
    if (filename.Contains("ZCalib-151617")) ra->DrawLatex(0.12,0.90,"#bf{#it{ATLAS Internal}}, #sqrt{s} = 13 TeV, 80.4 fb^{-1}");
    if (filename.Contains("ZCalib-fullrun2")) ra->DrawLatex(0.12,0.90,"#bf{#it{ATLAS Internal}}, #sqrt{s} = 13 TeV, 138.9 fb^{-1}");
    if (filename.Contains("VCalib")) ra->DrawLatex(0.12,0.90,"#bf{#it{ATLAS Internal}}, #sqrt{s} = 13 TeV, 138.9 fb^{-1}");

    TLatex * cal = new TLatex();
    cal->SetNDC();
    cal->SetTextFont(42);
    cal->SetTextColor(1);
    cal->SetTextSize(0.03);
    if (filename.Contains("ZCalib")) cal->DrawLatex(0.12,0.90-0.04,"Z#rightarrow b#bar{b} +jets calibration");
    if (filename.Contains("VCalib")) cal->DrawLatex(0.12,0.90-0.04,"V +jets calibration");

    TLatex * Tag = new TLatex();
    Tag->SetNDC();
    Tag->SetTextFont(42);
    Tag->SetTextColor(1);
    Tag->SetTextSize(0.03);
    if (filename.Contains("ZCalib"))  Tag->DrawLatex(0.12,0.90-0.04*2,"Xbb tagger: 60%");
    if (filename.Contains("VCalib"))  Tag->DrawLatex(0.12,0.90-0.04*2,"Smooth W/Z tagger: 50%");

    TLatex *ptbins = new TLatex();
    ptbins->SetNDC();
    ptbins->SetTextColor(kBlack);
    ptbins->SetTextFont(42);
    ptbins->SetTextSize(0.030);
    if(filename.Contains("450-500")) ptbins->DrawLatex(0.5,0.85,Form("%1d #leq p_{T} < %1d GeV",450,500));
    if(filename.Contains("500-600")) ptbins->DrawLatex(0.5,0.85,Form("%1d #leq p_{T} < %1d GeV",500,600));
    if(filename.Contains("600-700")) ptbins->DrawLatex(0.5,0.85,Form("%1d #leq p_{T} < %1d GeV",600,700));
    if(filename.Contains("700-800")) ptbins->DrawLatex(0.5,0.85,Form("%1d #leq p_{T} < %1d GeV",700,800));
    if(filename.Contains("700-1000")) ptbins->DrawLatex(0.5,0.85,Form("%1d #leq p_{T} < %1d GeV",700,1000));
    if(filename.Contains("800-1000")) ptbins->DrawLatex(0.5,0.85,Form("%1d #leq p_{T} < %1d GeV",800,1000));
    if(filename.Contains("1000-1500")) ptbins->DrawLatex(0.5,0.85,Form("%1d #leq p_{T} < %1d GeV",1000,1500));

    c->SaveAs(filename+"_"+_Calib+"_"+_process+"_DSCBm.pdf");
  }
}
  /* TCanvas* c1 = new TCanvas("flip","flip",800,1200);
  TImage *img = TImage::Create();
  img->FromPad(c);
  img->Flip(270);
  c1->cd();
  img->Draw("x");*/

