#ifndef TEMPLATEXML_HEADER
#define TEMPLATEXML_HEADER

#include "caliConfig.h"
#include "getData.h"
#include "fitTemplate.h"
#include "createXML.h"
#include <map>

using namespace std;

class templateXML : public TObject{
 public:
  static void getSglFit(TString ptbin,TString whichsystematic);
  static void getSglSystXML(TString ptbin,TString whichsystematic,map<TString, double> yields,TString bkgorder);
  static void getAllSystXML(TString ptbin,map<TString, double> yields,TString bkgorder);
  ClassDef(templateXML,1);
};

#endif
