#include "createXML.h"
#include "caliConfig.h"
#include "fitTemplate.h"
#include "TSystem.h"
#include "TH1F.h"

ClassImp(createXML)
void createXML::createQCDPdfXML(TString ptbin,TString whichorder){
  TXMLEngine* xml = new TXMLEngine();
  XMLNodePointer_t mainnode = xml->NewChild(0, 0, "Model");
  xml->NewAttr(mainnode, 0, "Type","UserDef");
  XMLNodePointer_t child0 = xml->NewChild(mainnode, 0, "ModelItem");
  if(whichorder == "1") xml->NewAttr(child0, 0, "Name","EXPR::bkgPdf('TMath::Exp(@1*TMath::Power((@0-50.)/100.,1)+@2*TMath::Power((@0-50.)/100.,2)+@3*TMath::Power((@0-50.)/100.,3)+@4*TMath::Power((@0-50.)/100.,4)+@5*TMath::Power((@0-50.)/100.,5))',:observable:,c[-0.0203,-100000,100000],d[0],e[0],f[0],g[0])");
  if(whichorder == "2") xml->NewAttr(child0, 0, "Name","EXPR::bkgPdf('TMath::Exp(@1*TMath::Power((@0-50.)/100.,1)+@2*TMath::Power((@0-50.)/100.,2)+@3*TMath::Power((@0-50.)/100.,3)+@4*TMath::Power((@0-50.)/100.,4)+@5*TMath::Power((@0-50.)/100.,5))',:observable:,c[-0.020355,-100000,100000],d[1.1691,-100000,100000],e[0],f[0],g[0])");
  if(whichorder == "3") xml->NewAttr(child0, 0, "Name","EXPR::bkgPdf('TMath::Exp(@1*TMath::Power((@0-50.)/100.,1)+@2*TMath::Power((@0-50.)/100.,2)+@3*TMath::Power((@0-50.)/100.,3)+@4*TMath::Power((@0-50.)/100.,4)+@5*TMath::Power((@0-50.)/100.,5))',:observable:,c[-0.020355,-100000,100000],d[1.1691,-100000,100000],e[-0.29822,-10000,10000],f[0],g[0])");
  if(whichorder == "4") xml->NewAttr(child0, 0, "Name","EXPR::bkgPdf('TMath::Exp(@1*TMath::Power((@0-50.)/100.,1)+@2*TMath::Power((@0-50.)/100.,2)+@3*TMath::Power((@0-50.)/100.,3)+@4*TMath::Power((@0-50.)/100.,4)+@5*TMath::Power((@0-50.)/100.,5))',:observable:,c[-0.020355,-100000,100000],d[1.1691,-100000,100000],e[-0.29822,-10000,10000],f[-0.51356,-10000,10000],g[0])");
  if(whichorder == "5") xml->NewAttr(child0, 0, "Name","EXPR::bkgPdf('TMath::Exp(@1*TMath::Power((@0-50.)/100.,1)+@2*TMath::Power((@0-50.)/100.,2)+@3*TMath::Power((@0-50.)/100.,3)+@4*TMath::Power((@0-50.)/100.,4)+@5*TMath::Power((@0-50.)/100.,5))',:observable:,c[-0.020355,-100000,100000],d[1.1691,-100000,100000],e[0],f[0],g[-0.56844,-10000,10000])");
  XMLDocPointer_t xmldoc = xml->NewDoc();
  xml->AddDocRawLine(xmldoc,"<!DOCTYPE Model SYSTEM 'AnaWSBuilder.dtd'>");
  xml->DocSetRootElement(xmldoc, mainnode);
  // Save document to file
  TString DirToSaveXML = caliConfig::_XMLOutputDir + caliConfig::_Calib + "/" + ptbin + "/model/";
  std::cout << "save xml to : " << DirToSaveXML << std::endl;
  gSystem->Exec("mkdir -p " + DirToSaveXML);
  xml->SaveDoc(xmldoc, DirToSaveXML+"SR_qcd_expoly"+whichorder+".xml");
  // Release memory before exit
  xml->FreeDoc(xmldoc);
  delete xml;
}
void createXML::createDSCBPdfXML(TString systematic,TString ptbin,TString whichprocess,map<TString, double> DSCBparas){

  TString muCB = to_string(DSCBparas["mu"]);
  TString sigmaCB = to_string(DSCBparas["sigma"]);
  TString alphaCBLo = to_string(DSCBparas["alphaLo"]);
  TString alphaCBHi = to_string(DSCBparas["alphaHi"]);
  TString nCBLo = to_string(DSCBparas["nLo"]);
  TString nCBHi = to_string(DSCBparas["nHi"]);

  // First create engine
  TXMLEngine* xml = new TXMLEngine();
  // Create main node of document tree
  XMLNodePointer_t mainnode = xml->NewChild(0, 0, "Model");
  xml->NewAttr(mainnode, 0, "Type","UserDef");
  //xml->NewAttr(mainnode, 0, "CacheBinning","100000");
  // Other child node with attributes
  
  XMLNodePointer_t child1z = xml->NewChild(mainnode, 0, "Item");
  xml->NewAttr(child1z, 0, "Name","muCB"+whichprocess+"["+muCB+"]");
  XMLNodePointer_t child2z = xml->NewChild(mainnode, 0, "Item");
  xml->NewAttr(child2z, 0, "Name","sigmaCB"+whichprocess+"["+sigmaCB+"]");
  XMLNodePointer_t child3z = xml->NewChild(mainnode, 0, "Item");
  xml->NewAttr(child3z, 0, "Name","alphaCBLo"+whichprocess+"["+alphaCBLo+"]");
  XMLNodePointer_t child4z = xml->NewChild(mainnode, 0, "Item");
  xml->NewAttr(child4z, 0, "Name","alphaCBHi"+whichprocess+"["+alphaCBHi+"]");
  XMLNodePointer_t child5z = xml->NewChild(mainnode, 0, "Item");
  xml->NewAttr(child5z, 0, "Name","nCBLo"+whichprocess+"["+nCBLo+"]");
  XMLNodePointer_t child6z = xml->NewChild(mainnode, 0, "Item");
  xml->NewAttr(child6z, 0, "Name","nCBHi"+whichprocess+"["+nCBHi+"]");

  XMLNodePointer_t child0z = xml->NewChild(mainnode, 0, "ModelItem");
  xml->NewAttr(child0z, 0, "Name","RooTwoSidedCBShape::Template"+whichprocess+"(:observable:,muCB"+whichprocess+", sigmaCB"+whichprocess+", alphaCBLo"+whichprocess+", nCBLo"+whichprocess+", alphaCBHi"+whichprocess+", nCBHi"+whichprocess+")");
  XMLDocPointer_t xmldoc = xml->NewDoc();
  xml->AddDocRawLine(xmldoc,"<!DOCTYPE Model SYSTEM 'AnaWSBuilder.dtd'>");
  xml->DocSetRootElement(xmldoc, mainnode);
  // Save document to file
  TString DirToSaveXML = caliConfig::_XMLOutputDir + caliConfig::_Calib + "/" + ptbin + "/model/";
  std::cout << "save xml to : " << DirToSaveXML << std::endl;
  gSystem->Exec("mkdir -p " + DirToSaveXML);
  xml->SaveDoc(xmldoc, DirToSaveXML+whichprocess+"_"+systematic+".xml");
  // Release memory before exit
  xml->FreeDoc(xmldoc);
  delete xml;
}
//void createQCDPdfXML(TString ptbin,TString whichorder){
//}
void createXML::createCategoryXML(map<TString, double> yields,TString systematic, TString ptbin, TString bkgorder, TString  xmlfilename){

  int index_pt = caliConfig::getIndex(caliConfig::_pTBins,ptbin);
  TString lo_pt = to_string(caliConfig::_pTCuts[index_pt]);
  TString hi_pt = to_string(caliConfig::_pTCuts[index_pt+1]);

  // First create engine
  TXMLEngine* xml = new TXMLEngine();
  // Create main node of document tree
  XMLNodePointer_t mainnode = xml->NewChild(0, 0, "Channel");
  xml->NewAttr(mainnode, 0, "Name","Full");
  xml->NewAttr(mainnode, 0, "Type","shape");
  xml->NewAttr(mainnode, 0, "Lumi","1");
  XMLNodePointer_t childData = xml->NewChild(mainnode, 0, "Data");
  xml->NewAttr(childData, 0, "Observable","mass[50,150]");
  xml->NewAttr(childData, 0, "FileType","ntuple");
  xml->NewAttr(childData, 0, "TreeName",caliConfig::_TreeName);
  xml->NewAttr(childData, 0, "VarName",caliConfig::_Category+"_mass");
  if(caliConfig::_Calib.Contains("ZCalib")){
    xml->NewAttr(childData, 0, "Cut",caliConfig::_Category+"_pt:ge:"+lo_pt+":and:"+caliConfig::_Category+"_pt:lt:"+hi_pt+":and:"+caliConfig::_Category+"_nVRjets:ge:2:and:"+caliConfig::_Category+"_isXbb_60:ge:1");
  }else if(caliConfig::_Calib.Contains("VCalib")){
    TString probe_s = caliConfig::_Category;
    if(caliConfig::_Category == "lj1") probe_s = "lj2";
    if(caliConfig::_Category == "lj2") probe_s = "lj1";
    xml->NewAttr(childData, 0, "Cut",caliConfig::_Category+"_pt:ge:"+lo_pt+":and:"+caliConfig::_Category+"_pt:lt:"+hi_pt+":and:ptAsy:le:0.15:and:dy:le:1.2:and:(("+caliConfig::_Category+"_isWTagged_50:ge:1:and:"+probe_s+"_passWD2_50:lt:1):or:("+caliConfig::_Category+"_isZTagged_50:ge:1:and:"+probe_s+"_passZD2_50:lt:1))");
  }
  xml->NewAttr(childData, 0, "Binning","100");
  xml->NewAttr(childData, 0, "InjectGhost","1");
  xml->NewAttr(childData, 0, "BlindRange","");
  xml->NewAttr(childData, 0, "InputFile",caliConfig::_InputDir+"data.root");
  //run2 luminosity systematic
  XMLNodePointer_t childSamplelumisyst = xml->NewChild(mainnode, 0, "Systematic");
  xml->NewAttr(childSamplelumisyst, 0, "Name","ATLAS_lumi_run2");
  xml->NewAttr(childSamplelumisyst, 0, "Constr","logn");
  xml->NewAttr(childSamplelumisyst, 0, "CentralValue","1");
  xml->NewAttr(childSamplelumisyst, 0, "Mag","0.02");
  xml->NewAttr(childSamplelumisyst, 0, "WhereTo","yield");
  //common ttbar systematic
  XMLNodePointer_t childSamplettbarsyst = xml->NewChild(mainnode, 0, "Systematic");
  xml->NewAttr(childSamplettbarsyst, 0, "Name","ATLAS_ttbar");
  xml->NewAttr(childSamplettbarsyst, 0, "Constr","gaus");
  xml->NewAttr(childSamplettbarsyst, 0, "CentralValue","1");
  xml->NewAttr(childSamplettbarsyst, 0, "Mag","0.15");
  xml->NewAttr(childSamplettbarsyst, 0, "WhereTo","yield");
  xml->NewAttr(childSamplettbarsyst, 0, "Process","ttbar_group");

  for(int ipp =0; ipp<caliConfig::_runProcess.size(); ipp++){
    XMLNodePointer_t childSample = xml->NewChild(mainnode, 0, "Sample");
    xml->NewAttr(childSample, 0, "Name",caliConfig::_runProcess[ipp]);
    xml->NewAttr(childSample, 0, "InputFile","model/"+caliConfig::_runProcess[ipp]+"_"+systematic+".xml");
   if(caliConfig::_runProcess[ipp].Contains("ttbar")){
      xml->NewAttr(childSample, 0, "ImportSyst",":common:,ttbar_group");
    }else{
      xml->NewAttr(childSample, 0, "ImportSyst",":common:");
    }
    xml->NewAttr(childSample, 0, "MultiplyLumi","1");
    XMLNodePointer_t childSample1 = xml->NewChild(childSample, 0, "NormFactor");
    TString yieldstring = to_string(yields[caliConfig::_runProcess[ipp]]);
    xml->NewAttr(childSample1,0, "Name","yield_"+caliConfig::_runProcess[ipp]+"["+yieldstring+"]");
    XMLNodePointer_t childSample2 = xml->NewChild(childSample, 0, "NormFactor");
    xml->NewAttr(childSample2,0, "Name","mu_"+caliConfig::_runProcess[ipp]+"[1]");
    XMLNodePointer_t childSample3 = xml->NewChild(childSample, 0, "NormFactor");
    if(caliConfig::_runProcess[ipp].Contains("ttbar")){
      xml->NewAttr(childSample3,0, "Name","mu_ttbar[1]");
      }else if(caliConfig::_Calib.Contains("VCalib") && (caliConfig::_runProcess[ipp].Contains("wqq") || caliConfig::_runProcess[ipp].Contains("zqq"))){
	xml->NewAttr(childSample3,0, "Name","mu[1,-5,5]");
    }else if(caliConfig::_Calib.Contains("ZCalib") && (caliConfig::_runProcess[ipp].Contains("zbb"))){
    xml->NewAttr(childSample3,0, "Name","mu[1,-5,5]");
    }else{
      xml->NewAttr(childSample3,0, "Name","mu[1,-5,5]");
      }
  }
  XMLNodePointer_t childbkg = xml->NewChild(mainnode, 0, "Sample");
  xml->NewAttr(childbkg, 0, "Name","bkgPdf");
  if(bkgorder.Contains("1")) xml->NewAttr(childbkg, 0, "InputFile","model/SR_qcd_expoly1.xml");
  if(bkgorder.Contains("2")) xml->NewAttr(childbkg, 0, "InputFile","model/SR_qcd_expoly2.xml");
  if(bkgorder.Contains("3")) xml->NewAttr(childbkg, 0, "InputFile","model/SR_qcd_expoly3.xml");
  if(bkgorder.Contains("4")) xml->NewAttr(childbkg, 0, "InputFile","model/SR_qcd_expoly4.xml");
  if(bkgorder.Contains("5")) xml->NewAttr(childbkg, 0, "InputFile","model/SR_qcd_expoly5.xml");
  if(bkgorder.Contains("6")) xml->NewAttr(childbkg, 0, "InputFile","model/SR_qcd_expoly6.xml");
  xml->NewAttr(childbkg, 0, "MultiplyLumi","1");
  XMLNodePointer_t childbkg1 = xml->NewChild(childbkg, 0, "NormFactor");
  xml->NewAttr(childbkg1,0, "Name","yield_bkg[10000,-10000000,1000000000]");
  XMLDocPointer_t xmldoc = xml->NewDoc();
  xml->AddDocRawLine(xmldoc,"<!DOCTYPE Channel SYSTEM 'AnaWSBuilder.dtd'>");
  xml->DocSetRootElement(xmldoc, mainnode);
  // Save document to file
  TString DirToSaveXML = caliConfig::_XMLOutputDir + caliConfig::_Calib + "/" + ptbin + "/";
  std::cout << "save xml to : " << DirToSaveXML << std::endl;
  xml->SaveDoc(xmldoc, DirToSaveXML + xmlfilename);
  // Release memory before exit
  xml->FreeDoc(xmldoc);
  delete xml;
}
void createXML::createTopXML(TString systematic, TString ptbin, TString bkgorder, TString xmlfilename){
  // First create engine
  TXMLEngine* xml = new TXMLEngine();
  // Create main node of document tree
  XMLNodePointer_t mainnode = xml->NewChild(0, 0, "Combination");
  xml->NewAttr(mainnode, 0, "WorkspaceName","combWS");
  xml->NewAttr(mainnode, 0, "ModelConfigName","ModelConfig");
  xml->NewAttr(mainnode, 0, "DataName","combData");
  TString DirToSaveWS = caliConfig::_XMLOutputDir + caliConfig::_Calib + "/" + ptbin + "/RooWorkspace/";
  gSystem->Exec("mkdir -p " + DirToSaveWS);
  xml->NewAttr(mainnode, 0, "OutputFile","RooWorkspace/SRdata_"+bkgorder+"_"+systematic+".root");
  xml->NewAttr(mainnode, 0, "Blind", "false");
  if(bkgorder.Contains("1")) XMLNodePointer_t childInput1 = xml->NewChild(mainnode, 0, "Input", "bkg1_"+systematic+"_c.xml");
  if(bkgorder.Contains("2")) XMLNodePointer_t childInput2 = xml->NewChild(mainnode, 0, "Input", "bkg2_"+systematic+"_c.xml");
  if(bkgorder.Contains("3")) XMLNodePointer_t childInput3 = xml->NewChild(mainnode, 0, "Input", "bkg3_"+systematic+"_c.xml");
  if(bkgorder.Contains("4")) XMLNodePointer_t childInput4 = xml->NewChild(mainnode, 0, "Input", "bkg4_"+systematic+"_c.xml");
  if(bkgorder.Contains("5")) XMLNodePointer_t childInput5 = xml->NewChild(mainnode, 0, "Input", "bkg5_"+systematic+"_c.xml");
  if(bkgorder.Contains("6")) XMLNodePointer_t childInput6 = xml->NewChild(mainnode, 0, "Input", "bkg6_"+systematic+"_c.xml");
  
  XMLNodePointer_t childPOI = xml->NewChild(mainnode, 0, "POI", "mu");
  //  XMLNsPointer_t nsPOI = xml->NewNS(childPOI, "mu_V");
  XMLDocPointer_t xmldoc = xml->NewDoc();
  xml->AddDocRawLine(xmldoc,"<!DOCTYPE Combination SYSTEM 'AnaWSBuilder.dtd'>");
  xml->DocSetRootElement(xmldoc, mainnode);
  // Save document to file  
  TString DirToSaveXML = caliConfig::_XMLOutputDir + caliConfig::_Calib + "/" + ptbin + "/";
  std::cout << "save xml to : " << DirToSaveXML << std::endl;
  xml->SaveDoc(xmldoc, DirToSaveXML + xmlfilename);
  // Release memory before exit
  xml->FreeDoc(xmldoc);
  delete xml;
}

void createXML::createAllSystDSCBPdfXML(TString ptbin,TString whichprocess, map<TString, double> DSCBparas){
  TString muCB = to_string(DSCBparas["mu"]);
  TString sigmaCB = to_string(DSCBparas["sigma"]);
  TString alphaCBLo = to_string(DSCBparas["alphaLo"]);
  TString alphaCBHi = to_string(DSCBparas["alphaHi"]);
  TString nCBLo = to_string(DSCBparas["nLo"]);
  TString nCBHi = to_string(DSCBparas["nHi"]);

  // First create engine
  TXMLEngine* xml = new TXMLEngine();
  // Create main node of document tree
  XMLNodePointer_t mainnode = xml->NewChild(0, 0, "Model");
  xml->NewAttr(mainnode, 0, "Type","UserDef");
  //xml->NewAttr(mainnode, 0, "CacheBinning","100000");
  // Other child node with attributes

  XMLNodePointer_t child1z = xml->NewChild(mainnode, 0, "Item");
  xml->NewAttr(child1z, 0, "Name","muCBNom"+whichprocess+"["+muCB+"]");
  XMLNodePointer_t child1z1 = xml->NewChild(mainnode, 0, "Item");
  TString sJMS = "";
  TString sJES = "";
  for(int isyst=0;isyst<caliConfig::_Systematics.size();isyst++){
    if(caliConfig::_Systematics[isyst].Contains("CategoryReduction_JET_CombMass")) {
      if(sJMS.IsNull()){ 
	sJMS = sJMS+"response::ATLAS_"+caliConfig::_Systematics[isyst]+":process:";
      }else{
	sJMS = sJMS+",response::ATLAS_"+caliConfig::_Systematics[isyst]+":process:";
      }
    }
    if(caliConfig::_Systematics[isyst].Contains("CategoryReduction_JET_EffectiveNP_R10") || caliConfig::_Systematics[isyst].Contains("CategoryReduction_JET_EtaIntercalibration") || caliConfig::_Systematics[isyst].Contains("CategoryReduction_JET_Flavor") || caliConfig::_Systematics[isyst].Contains("CategoryReduction_JET_LargeR")) {
      if(sJES.IsNull()){
	sJES = sJES+"response::ATLAS_"+caliConfig::_Systematics[isyst]+":process:";
      }else{
	sJES = sJES+",response::ATLAS_"+caliConfig::_Systematics[isyst]+":process:";
      }
    }
  }
  xml->NewAttr(child1z1, 0, "Name","prod::res_JMS:process:("+sJMS+")");
  XMLNodePointer_t child1z2 = xml->NewChild(mainnode, 0, "Item");
  xml->NewAttr(child1z2, 0, "Name","prod::res_JES:process:("+sJES+")");
  XMLNodePointer_t child1z0 = xml->NewChild(mainnode, 0, "Item");
  if(whichprocess.Contains("ttbar")){
    xml->NewAttr(child1z0, 0, "Name","prod::muCB"+whichprocess+"(muCBNom"+whichprocess+",res_JMS:process:,res_JES:process:,response::ATLAS_CategoryReduction_JET_SingleParticle_HighPt:process:)");
  }else{
    xml->NewAttr(child1z0, 0, "Name","prod::muCB"+whichprocess+"(muCBNom"+whichprocess+",res_JMS:process:,res_JES:process:,response::ATLAS_CategoryReduction_JET_SingleParticle_HighPt:process:,response::sh228lund:process:)");
  }
  XMLNodePointer_t child2z = xml->NewChild(mainnode, 0, "Item");
  xml->NewAttr(child2z, 0, "Name","sigmaCBNom"+whichprocess+"["+sigmaCB+"]");
  XMLNodePointer_t child2z1 = xml->NewChild(mainnode, 0, "Item");
  if(whichprocess.Contains("ttbar")){
    xml->NewAttr(child2z1, 0, "Name","prod::sigmaCB"+whichprocess+"(sigmaCBNom"+whichprocess+",response::ATLAS_CategoryReduction_JET_MassRes_XX_comb:process:)");
  }else{
    xml->NewAttr(child2z1, 0, "Name","prod::sigmaCB"+whichprocess+"(sigmaCBNom"+whichprocess+",response::ATLAS_CategoryReduction_JET_MassRes_XX_comb:process:)");
  }
  XMLNodePointer_t child3z = xml->NewChild(mainnode, 0, "Item");
  xml->NewAttr(child3z, 0, "Name","alphaCBLo"+whichprocess+"["+alphaCBLo+"]");
  XMLNodePointer_t child4z = xml->NewChild(mainnode, 0, "Item");
  xml->NewAttr(child4z, 0, "Name","alphaCBHi"+whichprocess+"["+alphaCBHi+"]");
  XMLNodePointer_t child5z = xml->NewChild(mainnode, 0, "Item");
  xml->NewAttr(child5z, 0, "Name","nCBLo"+whichprocess+"["+nCBLo+"]");
  XMLNodePointer_t child6z = xml->NewChild(mainnode, 0, "Item");
  xml->NewAttr(child6z, 0, "Name","nCBHi"+whichprocess+"["+nCBHi+"]");

  XMLNodePointer_t child0z = xml->NewChild(mainnode, 0, "ModelItem");
  xml->NewAttr(child0z, 0, "Name","RooTwoSidedCBShape::Template"+whichprocess+"(:observable:,muCB"+whichprocess+", sigmaCB"+whichprocess+", alphaCBLo"+whichprocess+", nCBLo"+whichprocess+", alphaCBHi"+whichprocess+", nCBHi"+whichprocess+")");
  XMLDocPointer_t xmldoc = xml->NewDoc();
  xml->AddDocRawLine(xmldoc,"<!DOCTYPE Model SYSTEM 'AnaWSBuilder.dtd'>");
  xml->DocSetRootElement(xmldoc, mainnode);
  // Save document to file
  TString DirToSaveXML = caliConfig::_XMLOutputDir + caliConfig::_Calib + "/" + ptbin + "/model/";
  std::cout << "save xml to : " << DirToSaveXML << std::endl;
  gSystem->Exec("mkdir -p " + DirToSaveXML);
  xml->SaveDoc(xmldoc, DirToSaveXML+whichprocess+"_allsyst.xml");
  // Release memory before exit
  xml->FreeDoc(xmldoc);
  delete xml;
}
void createXML::createAllSystCategoryXML(map<TString, double> yields,TString ptbin, TString bkgorder, TString xmlfilename){
  int index_pt = caliConfig::getIndex(caliConfig::_pTBins,ptbin);
  TString lo_pt = to_string(caliConfig::_pTCuts[index_pt]);
  TString hi_pt = to_string(caliConfig::_pTCuts[index_pt+1]);

  map<TString,TH1F*> h_syst_mu_down;
  map<TString,TH1F*> h_syst_mu_up;
  map<TString,TH1F*> h_syst_sigma_down;
  map<TString,TH1F*> h_syst_sigma_up;

  for(int ipp =0; ipp<caliConfig::_runProcess.size(); ipp++){
    h_syst_mu_down[caliConfig::_runProcess[ipp]] = new TH1F(caliConfig::_runProcess[ipp]+"_mu_down",caliConfig::_runProcess[ipp]+"_mu_down",caliConfig::_Systematics.size(),0,caliConfig::_Systematics.size());
    h_syst_mu_up[caliConfig::_runProcess[ipp]] = new TH1F(caliConfig::_runProcess[ipp]+"_mu_up",caliConfig::_runProcess[ipp]+"_mu_up",caliConfig::_Systematics.size(),0,caliConfig::_Systematics.size());
    h_syst_sigma_down[caliConfig::_runProcess[ipp]] = new TH1F(caliConfig::_runProcess[ipp]+"_sigma_down",caliConfig::_runProcess[ipp]+"_sigma_down",caliConfig::_Systematics.size(),0,caliConfig::_Systematics.size());
    h_syst_sigma_up[caliConfig::_runProcess[ipp]] = new TH1F(caliConfig::_runProcess[ipp]+"_sigma_up",caliConfig::_runProcess[ipp]+"_sigma_up",caliConfig::_Systematics.size(),0,caliConfig::_Systematics.size());
  }

  // First create engine
  TXMLEngine* xml = new TXMLEngine();
  // Create main node of document tree
  XMLNodePointer_t mainnode = xml->NewChild(0, 0, "Channel");
  xml->NewAttr(mainnode, 0, "Name","Full");
  xml->NewAttr(mainnode, 0, "Type","shape");
  xml->NewAttr(mainnode, 0, "Lumi","1");
  XMLNodePointer_t childData = xml->NewChild(mainnode, 0, "Data");
  xml->NewAttr(childData, 0, "Observable","mass[50,150]");
  xml->NewAttr(childData, 0, "FileType","ntuple");
  xml->NewAttr(childData, 0, "TreeName",caliConfig::_TreeName);
  xml->NewAttr(childData, 0, "VarName",caliConfig::_Category+"_mass");
  if(caliConfig::_Calib.Contains("ZCalib")){
    xml->NewAttr(childData, 0, "Cut",caliConfig::_Category+"_pt:ge:"+lo_pt+":and:"+caliConfig::_Category+"_pt:lt:"+hi_pt+":and:"+caliConfig::_Category+"_nVRjets:ge:2:and:"+caliConfig::_Category+"_isXbb_60:ge:1");
  }else if(caliConfig::_Calib.Contains("VCalib")){
    TString probe_s = caliConfig::_Category;
    if(caliConfig::_Category == "lj1") probe_s = "lj2";
    if(caliConfig::_Category == "lj2") probe_s = "lj1";
    xml->NewAttr(childData, 0, "Cut",caliConfig::_Category+"_pt:ge:"+lo_pt+":and:"+caliConfig::_Category+"_pt:lt:"+hi_pt+":and:ptAsy:le:0.15:and:dy:le:1.2:and:(("+caliConfig::_Category+"_isWTagged_50:ge:1:and:"+probe_s+"_passWD2_50:lt:1):or:("+caliConfig::_Category+"_isZTagged_50:ge:1:and:"+probe_s+"_passZD2_50:lt:1))");
  }
  xml->NewAttr(childData, 0, "Binning","100");
  xml->NewAttr(childData, 0, "InjectGhost","1");
  xml->NewAttr(childData, 0, "BlindRange","");
  xml->NewAttr(childData, 0, "InputFile",caliConfig::_InputDir+"data.root");
  //run2 luminosity systematic
  XMLNodePointer_t childSamplelumisyst = xml->NewChild(mainnode, 0, "Systematic");
  xml->NewAttr(childSamplelumisyst, 0, "Name","ATLAS_lumi_run2");
  xml->NewAttr(childSamplelumisyst, 0, "Constr","logn");
  xml->NewAttr(childSamplelumisyst, 0, "CentralValue","1");
  xml->NewAttr(childSamplelumisyst, 0, "Mag","0.02");
  xml->NewAttr(childSamplelumisyst, 0, "WhereTo","yield");
  //common ttbar systematic
  XMLNodePointer_t childSamplettbarsyst = xml->NewChild(mainnode, 0, "Systematic");
  xml->NewAttr(childSamplettbarsyst, 0, "Name","ATLAS_ttbar");
  xml->NewAttr(childSamplettbarsyst, 0, "Constr","gaus");
  xml->NewAttr(childSamplettbarsyst, 0, "CentralValue","1");
  xml->NewAttr(childSamplettbarsyst, 0, "Mag","0.15");
  xml->NewAttr(childSamplettbarsyst, 0, "WhereTo","yield");
  xml->NewAttr(childSamplettbarsyst, 0, "Process","ttbar_group");
  for(int ipp =0; ipp<caliConfig::_runProcess.size(); ipp++){
    map<TString, double> DSCBparasNom = fitTemplate::getDSCBParas(caliConfig::_FitOutputDir + caliConfig::_Calib + "/" + ptbin + "/fitresults/"+caliConfig::_runProcess[ipp]+"_nominal.root");
    //if(caliConfig::_Calib.Contains("ZCalib") && !caliConfig::_runProcess[ipp].Contains("zbb")) continue;
    for(int isyst = 0; isyst<caliConfig::_Systematics.size();isyst++){
      TString outfilename = caliConfig::_FitOutputDir + caliConfig::_Calib + "/" + ptbin + "/fitresults/"+caliConfig::_runProcess[ipp]+"_"+caliConfig::_Systematics[isyst];
      if(caliConfig::_Systematics[isyst].Contains("nominal")) continue;
      if(caliConfig::_Systematics[isyst].Contains("sh228lund")){
	if(caliConfig::_runProcess[ipp].Contains("ttbar")) continue;
	map<TString, double> systparas = fitTemplate::getDSCBParas(outfilename+".root");

	h_syst_mu_up[caliConfig::_runProcess[ipp]]->Fill(caliConfig::_Systematics[isyst],abs(DSCBparasNom["mu"]-systparas["mu"])/DSCBparasNom["mu"]);
	h_syst_sigma_up[caliConfig::_runProcess[ipp]]->Fill(caliConfig::_Systematics[isyst],abs(DSCBparasNom["sigma"]-systparas["sigma"])/DSCBparasNom["sigma"]);
	h_syst_mu_down[caliConfig::_runProcess[ipp]]->Fill(caliConfig::_Systematics[isyst],abs(DSCBparasNom["mu"]-systparas["mu"])/DSCBparasNom["mu"]);
        h_syst_sigma_down[caliConfig::_runProcess[ipp]]->Fill(caliConfig::_Systematics[isyst],abs(DSCBparasNom["sigma"]-systparas["sigma"])/DSCBparasNom["sigma"]);

	TString mulund = to_string(abs(DSCBparasNom["mu"]-systparas["mu"])/DSCBparasNom["mu"]);
        TString sigmalund = to_string(abs(DSCBparasNom["sigma"]-systparas["sigma"])/DSCBparasNom["sigma"]);
	XMLNodePointer_t childSamplesystItem1 = xml->NewChild(mainnode, 0, "Item");
        xml->NewAttr(childSamplesystItem1, 0, "Name",caliConfig::_Systematics[isyst]+"_muDSCB_"+caliConfig::_runProcess[ipp]+"["+mulund+"]");
	XMLNodePointer_t childSamplesystItem3 = xml->NewChild(mainnode, 0, "Item");
        xml->NewAttr(childSamplesystItem3, 0, "Name",caliConfig::_Systematics[isyst]+"_sigDSCB_"+caliConfig::_runProcess[ipp]+"["+sigmalund+"]");
      }else{
        map<TString, double> systparasup = fitTemplate::getDSCBParas(outfilename+"__1up.root");
        map<TString, double> systparasdown = fitTemplate::getDSCBParas(outfilename+"__1down.root");

	h_syst_mu_up[caliConfig::_runProcess[ipp]]->Fill(caliConfig::_Systematics[isyst],(systparasup["mu"]-DSCBparasNom["mu"])/DSCBparasNom["mu"]);
        h_syst_sigma_up[caliConfig::_runProcess[ipp]]->Fill(caliConfig::_Systematics[isyst],(systparasup["sigma"]-DSCBparasNom["sigma"])/DSCBparasNom["sigma"]);
	h_syst_mu_down[caliConfig::_runProcess[ipp]]->Fill(caliConfig::_Systematics[isyst],(DSCBparasNom["mu"]-systparasdown["mu"])/DSCBparasNom["mu"]);
	h_syst_sigma_down[caliConfig::_runProcess[ipp]]->Fill(caliConfig::_Systematics[isyst],(DSCBparasNom["sigma"]-systparasdown["sigma"])/DSCBparasNom["sigma"]);


        TString mulo = to_string((DSCBparasNom["mu"]-systparasdown["mu"])/DSCBparasNom["mu"]);
        TString sigmalo = to_string((DSCBparasNom["sigma"]-systparasdown["sigma"])/DSCBparasNom["sigma"]);
        TString muhi = to_string((systparasup["mu"]-DSCBparasNom["mu"])/DSCBparasNom["mu"]);
        TString sigmahi = to_string((systparasup["sigma"]-DSCBparasNom["sigma"])/DSCBparasNom["sigma"]);
        XMLNodePointer_t childSamplesystItem1 = xml->NewChild(mainnode, 0, "Item");
	xml->NewAttr(childSamplesystItem1, 0, "Name",caliConfig::_Systematics[isyst]+"_muDSCBDn_"+caliConfig::_runProcess[ipp]+"["+mulo+"]");
	XMLNodePointer_t childSamplesystItem2 = xml->NewChild(mainnode, 0, "Item");
	xml->NewAttr(childSamplesystItem2, 0, "Name",caliConfig::_Systematics[isyst]+"_muDSCBUp_"+caliConfig::_runProcess[ipp]+"["+muhi+"]");
	XMLNodePointer_t childSamplesystItem3 = xml->NewChild(mainnode, 0, "Item");
	xml->NewAttr(childSamplesystItem3, 0, "Name",caliConfig::_Systematics[isyst]+"_sigDSCBDn_"+caliConfig::_runProcess[ipp]+"["+sigmalo+"]");
        XMLNodePointer_t childSamplesystItem4 = xml->NewChild(mainnode, 0, "Item");
	xml->NewAttr(childSamplesystItem4, 0, "Name",caliConfig::_Systematics[isyst]+"_sigDSCBUp_"+caliConfig::_runProcess[ipp]+"["+sigmahi+"]");
      }
    }
  }
  for(int ipp =0; ipp<caliConfig::_runProcess.size(); ipp++){
    XMLNodePointer_t childSample = xml->NewChild(mainnode, 0, "Sample");
    xml->NewAttr(childSample, 0, "Name",caliConfig::_runProcess[ipp]);
    xml->NewAttr(childSample, 0, "InputFile","model/"+caliConfig::_runProcess[ipp]+"_allsyst.xml");
    if(caliConfig::_runProcess[ipp].Contains("ttbar")){
      xml->NewAttr(childSample, 0, "ImportSyst",":common:,ttbar_group");
    }else{
      xml->NewAttr(childSample, 0, "ImportSyst",":common:");
    }
    xml->NewAttr(childSample, 0, "MultiplyLumi","1");
    //add systematic here
    for(int isyst = 0; isyst<caliConfig::_Systematics.size();isyst++){
      if(caliConfig::_Systematics[isyst].Contains("nominal")) continue;
      if(caliConfig::_Systematics[isyst].Contains("CategoryReduction_JET_MassRes")) continue;
      if(caliConfig::_Systematics[isyst].Contains("sh228lund") && caliConfig::_runProcess[ipp].Contains("ttbar")) continue;
      XMLNodePointer_t childSamplesyst = xml->NewChild(childSample, 0, "Systematic");
      if(caliConfig::_Systematics[isyst].Contains("sh228lund")){
	xml->NewAttr(childSamplesyst, 0, "Name",caliConfig::_Systematics[isyst]);
        xml->NewAttr(childSamplesyst, 0, "Constr","gaus");
        xml->NewAttr(childSamplesyst, 0, "CentralValue","1");
	xml->NewAttr(childSamplesyst, 0, "Mag", caliConfig::_Systematics[isyst]+"_muDSCB_"+caliConfig::_runProcess[ipp]);
      }else{
	xml->NewAttr(childSamplesyst, 0, "Name","ATLAS_"+caliConfig::_Systematics[isyst]);
	xml->NewAttr(childSamplesyst, 0, "Constr","asym");
	xml->NewAttr(childSamplesyst, 0, "CentralValue","1");
	xml->NewAttr(childSamplesyst, 0, "Mag",caliConfig::_Systematics[isyst]+"_muDSCBUp_"+caliConfig::_runProcess[ipp]+","+caliConfig::_Systematics[isyst]+"_muDSCBDn_"+caliConfig::_runProcess[ipp]);
      }
	xml->NewAttr(childSamplesyst, 0, "WhereTo","shape");
    } //end systematic

    //special for JMR
    XMLNodePointer_t childSamplesystJMR = xml->NewChild(childSample, 0, "Systematic");
    xml->NewAttr(childSamplesystJMR, 0, "Name","ATLAS_CategoryReduction_JET_MassRes_XX_comb");
    xml->NewAttr(childSamplesystJMR, 0, "Constr","gaus");
    xml->NewAttr(childSamplesystJMR, 0, "CentralValue","1");
    if(caliConfig::_runProcess[ipp].Contains("ttbar")){
      xml->NewAttr(childSamplesystJMR, 0, "Mag","CategoryReduction_JET_MassRes_Top_comb_sigDSCBDn_"+caliConfig::_runProcess[ipp]);
    }else{
      xml->NewAttr(childSamplesystJMR, 0, "Mag","CategoryReduction_JET_MassRes_WZ_comb_sigDSCBDn_"+caliConfig::_runProcess[ipp]);
    }
    xml->NewAttr(childSamplesystJMR, 0, "WhereTo","shape");


    XMLNodePointer_t childSample1 = xml->NewChild(childSample, 0, "NormFactor");
    TString yieldstring = to_string(yields[caliConfig::_runProcess[ipp]]);
    xml->NewAttr(childSample1,0, "Name","yield_"+caliConfig::_runProcess[ipp]+"["+yieldstring+"]");
    XMLNodePointer_t childSample2 = xml->NewChild(childSample, 0, "NormFactor");
    xml->NewAttr(childSample2,0, "Name","mu_"+caliConfig::_runProcess[ipp]+"[1]");
    XMLNodePointer_t childSample3 = xml->NewChild(childSample, 0, "NormFactor");
    if(caliConfig::_runProcess[ipp].Contains("ttbar")){
      xml->NewAttr(childSample3,0, "Name","mu_ttbar[1]");
    }else if(caliConfig::_Calib.Contains("VCalib") && (caliConfig::_runProcess[ipp].Contains("wqq") || caliConfig::_runProcess[ipp].Contains("zqq"))){
      xml->NewAttr(childSample3,0, "Name","mu[1,-5,5]");
    }else if(caliConfig::_Calib.Contains("ZCalib") && (caliConfig::_runProcess[ipp].Contains("zbb"))){
      xml->NewAttr(childSample3,0, "Name","mu[1,-5,5]");
    }else{
      xml->NewAttr(childSample3,0, "Name","mu[1,-5,5]");
    }
  }//end process

  XMLNodePointer_t childbkg = xml->NewChild(mainnode, 0, "Sample");
  xml->NewAttr(childbkg, 0, "Name","bkgPdf");
  if(bkgorder.Contains("1")) xml->NewAttr(childbkg, 0, "InputFile","model/SR_qcd_expoly1.xml");
  if(bkgorder.Contains("2")) xml->NewAttr(childbkg, 0, "InputFile","model/SR_qcd_expoly2.xml");
  if(bkgorder.Contains("3")) xml->NewAttr(childbkg, 0, "InputFile","model/SR_qcd_expoly3.xml");
  if(bkgorder.Contains("4")) xml->NewAttr(childbkg, 0, "InputFile","model/SR_qcd_expoly4.xml");
  if(bkgorder.Contains("5")) xml->NewAttr(childbkg, 0, "InputFile","model/SR_qcd_expoly5.xml");
  if(bkgorder.Contains("6")) xml->NewAttr(childbkg, 0, "InputFile","model/SR_qcd_expoly6.xml");
  xml->NewAttr(childbkg, 0, "MultiplyLumi","1");
  XMLNodePointer_t childbkg1 = xml->NewChild(childbkg, 0, "NormFactor");
  xml->NewAttr(childbkg1,0, "Name","yield_bkg[40000,-10000000,1000000000]");
  XMLDocPointer_t xmldoc = xml->NewDoc();
  xml->AddDocRawLine(xmldoc,"<!DOCTYPE Channel SYSTEM 'AnaWSBuilder.dtd'>");
  xml->DocSetRootElement(xmldoc, mainnode);
  // Save document to file
  TString DirToSaveXML = caliConfig::_XMLOutputDir + caliConfig::_Calib + "/" + ptbin + "/";
  std::cout << "save xml to : " << DirToSaveXML << std::endl;
  xml->SaveDoc(xmldoc, DirToSaveXML + xmlfilename);
  // Release memory before exit
  xml->FreeDoc(xmldoc);
  delete xml;

  TFile* f_hist = new TFile(DirToSaveXML+"syst_mag.root","RECREATE");
  for(int ipp =0; ipp<caliConfig::_runProcess.size(); ipp++){
    h_syst_mu_down[caliConfig::_runProcess[ipp]]->Write();
    h_syst_mu_up[caliConfig::_runProcess[ipp]]->Write();
    h_syst_sigma_down[caliConfig::_runProcess[ipp]]->Write();
    h_syst_sigma_up[caliConfig::_runProcess[ipp]]->Write();
  }
  f_hist->Close();
}

void createXML::createAllSystTopXML(TString ptbin, TString bkgorder, TString xmlfilename){
  // First create engine
  TXMLEngine* xml = new TXMLEngine();
  // Create main node of document tree
  XMLNodePointer_t mainnode = xml->NewChild(0, 0, "Combination");
  xml->NewAttr(mainnode, 0, "WorkspaceName","combWS");
  xml->NewAttr(mainnode, 0, "ModelConfigName","ModelConfig");
  xml->NewAttr(mainnode, 0, "DataName","combData");
  TString DirToSaveWS = caliConfig::_XMLOutputDir + caliConfig::_Calib + "/" + ptbin + "/RooWorkspace/";
  gSystem->Exec("mkdir -p " + DirToSaveWS);
  xml->NewAttr(mainnode, 0, "OutputFile","RooWorkspace/SRdata_"+bkgorder+"_allsyst.root");
  xml->NewAttr(mainnode, 0, "Blind", "false");
  if(bkgorder.Contains("1")) XMLNodePointer_t childInput1 = xml->NewChild(mainnode, 0, "Input", "bkg1_allsyst_c.xml");
  if(bkgorder.Contains("2")) XMLNodePointer_t childInput2 = xml->NewChild(mainnode, 0, "Input", "bkg2_allsyst_c.xml");
  if(bkgorder.Contains("3")) XMLNodePointer_t childInput3 = xml->NewChild(mainnode, 0, "Input", "bkg3_allsyst_c.xml");
  if(bkgorder.Contains("4")) XMLNodePointer_t childInput4 = xml->NewChild(mainnode, 0, "Input", "bkg4_allsyst_c.xml");
  if(bkgorder.Contains("5")) XMLNodePointer_t childInput5 = xml->NewChild(mainnode, 0, "Input", "bkg5_allsyst_c.xml");
  if(bkgorder.Contains("6")) XMLNodePointer_t childInput6 = xml->NewChild(mainnode, 0, "Input", "bkg6_allsyst_c.xml");

  XMLNodePointer_t childPOI = xml->NewChild(mainnode, 0, "POI", "mu");
  //  XMLNsPointer_t nsPOI = xml->NewNS(childPOI, "mu_V");
  XMLDocPointer_t xmldoc = xml->NewDoc();
  xml->AddDocRawLine(xmldoc,"<!DOCTYPE Combination SYSTEM 'AnaWSBuilder.dtd'>");
  xml->DocSetRootElement(xmldoc, mainnode);
  // Save document to file
  TString DirToSaveXML = caliConfig::_XMLOutputDir + caliConfig::_Calib + "/" + ptbin + "/";
  std::cout << "save xml to : " << DirToSaveXML << std::endl;
  xml->SaveDoc(xmldoc, DirToSaveXML + xmlfilename);
  // Release memory before exit
  xml->FreeDoc(xmldoc);
  delete xml;

}

