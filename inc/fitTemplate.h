#ifndef FITTEMPLATE_HEADER
#define FITTEMPLATE_HEADER


#include "RooTwoSidedCBShape.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooDataHist.h"
#include "RooPlot.h"
#include "RooFitResult.h"
#include "RooHist.h"

#include "caliConfig.h"
#include "getData.h"
#include <map>

using namespace std;
using namespace RooFit;

class fitTemplate : public TObject{
 public:
  // Minimizer setup
  static int _minimizerStrategy;
  static string _minimizerAlgo;
  static double _minimizerTolerance;
  static bool _nllOffset;
  static int _printLevel;

  static int doFit(TString filename, double ptmin, double ptmax, TString whichprocess, TString ptbin);
  static map<TString, double> getDSCBParas(TString pathtofitresult);
  static map<TString, map<TString, map<TString,double>>> getSystsMap(TString whichprocess,TString pathtofitresult); //(syst, DSCB paras)
  ClassDef(fitTemplate,1);
};

#endif
