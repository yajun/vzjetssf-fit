**Instructions of fit for V/Z+jets calibration**


Step1: create templates and xml file
==========================================

```
mkdir FOLDER-OF-YOUR-PROJECT
cd FOLDER-OF-YOUR-PROJECT
git clone https://gitlab.cern.ch/yajun/vzjetssf-fit.git
cd vzjetssf-fit
source setup_lxplus.sh
```
**Install RooFitExtensions**

If you have not installed RooFitExtensions, please install [RooFitExtension](https://gitlab.cern.ch/atlas_higgs_combination/software/RooFitExtensions).

```
cd ..
git clone https://gitlab.cern.ch/atlas_higgs_combination/software/RooFitExtensions.git
cd RooFitExtensions
mkdir build
cd build
cmake ..
make -j4
cd ..
source build/setup.sh
```

**Build**

```
cd ../vzjetssf-fit
mkdir build
cd build
cmake ..
make
cd ../
```

**Run**

```
MyCoolModel arg1 arg2 arg3
```
There are 4 options for arg1: **VCalib_lj1**(V+jets calibration using leading large-R jets), **VCalib_lj2**(V+jets calibration using subleading large-R jets), **ZCalib_lj1**(Zbb+jets calibration using leading large-R jets), **ZCalib_lj2**(Zbb+jets calibration using subleading large-R jets)

There are 3 options for arg2: **doFit** (fit templates only for nominal and syst. var.), **doXML** (create xml card for xmlAnaWSBuilder), **doBoth**. The xml cards cannot be created without the templates as inputs. 

The process is specified using arg3. "all" means all the process will be ran in the same time. More details could be found in `bin/MyCoolModel.cpp`

Next time when you use the framework, you just need to do:

```
source setup.sh
``` 
If you modify some files or add new files, you need to rebuild the framework.


Step2: create WS using [xmlAnaWSBuilder](https://gitlab.cern.ch/yajun/xmlAnaWSBuilder)
===========================================

**Setup xmlAnaWSBuilder**

Open a new shell, install [xmlAnaWSBuilder](https://gitlab.cern.ch/yajun/xmlAnaWSBuilder) according to the instruction. More info about xmlAnaWSBuilder can be found: https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/XmlAnaWSBuilder

**run xmlAnaWSBuilder**

```
source setup_xmlAnaWSBuilder.sh
cd your-directory-to-VZjetsSF-Fit
cd xml/[V/Z]Calib[lj1/lj2]/[ptbin]/
ln -s $_DIRXMLWSBUILDER/dtd/AnaWSBuilder.dtd .
ln -s $_DIRXMLWSBUILDER/dtd/AnaWSBuilder.dtd model/.
XMLReader -x your-top-level-xml-card
```
Then, you will get the RooWorkSpace for the final fit in ./RooWorkspace/

Step3: [quickFit](https://gitlab.cern.ch/yajun/quickFit)
==========================================
**Setup quickFit**

Open a new shell, install [quickFit](https://gitlab.cern.ch/yajun/quickFit) according to the instructions.

**run quickFit**

```
source setup_quickFit.sh
cd your-directory-to-VZjetsSF-Fit
cd xml/[V/Z]Calib[lj1/lj2]/[ptbin]/
mkdir fitResult
quickFit -f RooWorkspace/youR-ws-file.root -d combData -p mu=1_-5_5 -o fitResult/res.root --savefitresult 1 --hesse 1 --minos 1 --saveWS 1 --ssname quickfit
```

Plot
==========

**plot fit result**

```
root -l scripts/plotSRdata.C
```
**plot pull of NPs**

```
root -l scripts/plotPullofSysts.C
```
