#include "templateXML.h"
#include "caliConfig.h"

void templateXML::getSglFit(TString ptbin,TString whichsystematic){
  for (uint ip=0;ip<caliConfig::_runProcess.size();++ip){
    std::cout << "=======================Fit template for: "<< caliConfig::_runProcess[ip] << "==========================" << std::endl;
      TString runp = caliConfig::_runProcess[ip];
      //if (runp.Contains("zbb")) continue;
      //if (runp.Contains("ttbarCatTop")) continue;
      TString whichprocess = runp+"_"+whichsystematic;
      if(runp.Contains("ttbar")) runp = "ttbar";
      std::cout << "whichprocess: " << whichprocess << std::endl;
      if(runp.Contains("ttbar") && whichsystematic.Contains("sh228lund")) continue;
      int indexpt = caliConfig::getIndex(caliConfig::_pTBins,ptbin);
      int fitstatus = fitTemplate::doFit(runp+"-"+whichsystematic+".root",caliConfig::_pTCuts[indexpt],caliConfig::_pTCuts[indexpt+1],whichprocess,caliConfig::_pTBins[indexpt]);
      if(fitstatus == 0 ){
	std::cout << "=======================Fit succeed ==========================" << std::endl;
      }else{
	std::cout << "=======================Fit failed =========================="<< std::endl;
      }
      std::cout << "             Process: " + caliConfig::_runProcess[ip] << std::endl;
      std::cout << "             pTbin: " + caliConfig::_pTBins[indexpt] << std::endl;
      std::cout << "             Systematic: " + whichsystematic << std::endl;
      std::cout << "=============================================================="<< std::endl;
  }
}

void templateXML::getSglSystXML(TString ptbin,TString whichsystematic,map<TString, double> yields,TString bkgorder){
  if(caliConfig::_doFit){
    getSglFit(ptbin,whichsystematic);
  }
  for (uint ip=0;ip<caliConfig::_runProcess.size();++ip){
    if(caliConfig::_runProcess[ip].Contains("ttbar") && whichsystematic.Contains("sh228lund")) continue;
    std::cout << "create xml for process: " << caliConfig::_runProcess[ip] << std::endl; 
    map<TString, double> res_DSCB = fitTemplate::getDSCBParas(caliConfig::_FitOutputDir + caliConfig::_Calib + "/" + ptbin + "/fitresults/"+caliConfig::_runProcess[ip]+"_"+whichsystematic+".root");
    createXML::createDSCBPdfXML(whichsystematic,ptbin,caliConfig::_runProcess[ip],res_DSCB);
  }
  createXML::createQCDPdfXML(ptbin,bkgorder);
  createXML::createCategoryXML(yields, whichsystematic, ptbin, bkgorder, "bkg"+bkgorder+"_"+whichsystematic+"_c.xml");
  createXML::createTopXML(whichsystematic,ptbin, bkgorder, "bkg"+bkgorder+"_"+whichsystematic+".xml");
}

void templateXML::getAllSystXML(TString ptbin,map<TString, double> yields,TString bkgorder){
  for (uint ip=0;ip<caliConfig::_runProcess.size();++ip){
    std::cout << "create xml for process: " << caliConfig::_runProcess[ip] << std::endl;
    map<TString, double> res_DSCB = fitTemplate::getDSCBParas(caliConfig::_FitOutputDir + caliConfig::_Calib + "/" + ptbin + "/fitresults/"+caliConfig::_runProcess[ip]+"_nominal.root");
    createXML::createAllSystDSCBPdfXML(ptbin,caliConfig::_runProcess[ip],res_DSCB);
  }
  createXML::createQCDPdfXML(ptbin,bkgorder);
  createXML::createAllSystCategoryXML(yields, ptbin, bkgorder, "bkg"+bkgorder+"_allsyst_c.xml");
  createXML::createAllSystTopXML(ptbin, bkgorder, "bkg"+bkgorder+"_allsyst.xml");
}
