#ifndef GETDATA_HEADER
#define GETDATA_HEADER

#include "caliConfig.h"
#include "TROOT.h"
#include "TTree.h"
#include "TFile.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooDataHist.h"
#include "ROOT/TSeq.hxx"

#include <iostream>
#include <fstream>
#include <iostream>
#include <string>
#include <map>

using namespace RooFit;
using namespace std;

class getData : public TObject{
 public:
  //static TString _Calib; //define ZCalib or VCalib
  //static TString _InputDir; //
  //static TString _TreeName;
  static std::vector<TString> getSystsList();
  static RooDataSet* importdata(TString filename, double ptmin, double ptmax, TString whichprocess);
  static double getyield(TString filename, double ptmin, double ptmax, TString whichprocess);
  static map<TString, double> getyields(double ptmin, double ptmax);

  ClassDef(getData,1);
};

#endif
