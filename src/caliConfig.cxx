#include "caliConfig.h"

ClassImp(caliConfig)

TString caliConfig::_Calib = "VCalib";
bool caliConfig::_doFit = true;
TString caliConfig::_InputDir = "/sps/atlas/y/yahe/hadd/";
TString caliConfig::_TreeName = "Vtag";
TString caliConfig::_Category = "lj1";
TString caliConfig::_FitOutputDir = "/sps/atlas/y/yahe/Vjets-fit/templateFit/output/";
TString caliConfig::_XMLOutputDir = "/sps/atlas/y/yahe/Vjets-fit/templateFit/xml/";
std::vector<TString>  caliConfig::_runProcess = {};
std::vector<TString> caliConfig::_pTBins = {};
std::vector<double> caliConfig::_pTCuts = {};
std::vector<TString> caliConfig::_Systematics = {"nominal"};

/*
void caliConfig::initrunProcess(){
  if(_Calib.Contains("ZCalib")){
    _runProcess.push_back("zbb");
    // _runProcess.push_back("zqq"); don't consider the contribution of Zqq and Wqq for now
    // _runProcess.push_back("wqq");
    _runProcess.push_back("ttbarCatTop");
    _runProcess.push_back("ttbarCatWOther"); //combine W and Other as one
    //_runProcess.push_back("ttbarCatOther");
  }else if(_Calib.Contains("VCalib")){
    _runProcess.push_back("wqq");
    _runProcess.push_back("zqq");
    _runProcess.push_back("ttbarCatTop");
    _runProcess.push_back("ttbarCatW");
    _runProcess.push_back("ttbarCatOther");
  }
}


void caliConfig::initpTBins(){
  if(_Calib.Contains("ZCalib")){
    _pTBins.push_back("450-500");
    _pTBins.push_back("500-600");
    _pTBins.push_back("600-700");
    _pTBins.push_back("700-1000");
    _pTCuts.push_back(450.);
    _pTCuts.push_back(500.);
    _pTCuts.push_back(600.);
    _pTCuts.push_back(700.);
    _pTCuts.push_back(1000.);
  }else if(_Calib.Contains("VCalib")){
    _pTBins.push_back("500-600");
    _pTBins.push_back("600-700");
    _pTBins.push_back("700-800");
    _pTBins.push_back("800-1000");
    _pTBins.push_back("1000-1500");
    _pTCuts.push_back(500.);
    _pTCuts.push_back(600.);
    _pTCuts.push_back(700.);
    _pTCuts.push_back(800.);
    _pTCuts.push_back(1000.);
    _pTCuts.push_back(1500.);
  }
}
*/

void caliConfig::initSystematics(){
  _Systematics.push_back("sh228lund");
  _Systematics.push_back("CategoryReduction_JET_CombMass_Baseline");
  _Systematics.push_back("CategoryReduction_JET_CombMass_Modelling");
  _Systematics.push_back("CategoryReduction_JET_CombMass_TotalStat");
  _Systematics.push_back("CategoryReduction_JET_CombMass_Tracking1");
  _Systematics.push_back("CategoryReduction_JET_CombMass_Tracking2");
  _Systematics.push_back("CategoryReduction_JET_CombMass_Tracking3");
  _Systematics.push_back("CategoryReduction_JET_EffectiveNP_R10_Detector1");
  _Systematics.push_back("CategoryReduction_JET_EffectiveNP_R10_Detector2");
  _Systematics.push_back("CategoryReduction_JET_EffectiveNP_R10_Mixed1");
  _Systematics.push_back("CategoryReduction_JET_EffectiveNP_R10_Mixed2");
  _Systematics.push_back("CategoryReduction_JET_EffectiveNP_R10_Mixed3");
  _Systematics.push_back("CategoryReduction_JET_EffectiveNP_R10_Mixed4");
  _Systematics.push_back("CategoryReduction_JET_EffectiveNP_R10_Modelling1");//14
  _Systematics.push_back("CategoryReduction_JET_EffectiveNP_R10_Modelling2");
  _Systematics.push_back("CategoryReduction_JET_EffectiveNP_R10_Modelling3");
  _Systematics.push_back("CategoryReduction_JET_EffectiveNP_R10_Modelling4");
  _Systematics.push_back("CategoryReduction_JET_EffectiveNP_R10_Statistical1");
  _Systematics.push_back("CategoryReduction_JET_EffectiveNP_R10_Statistical2");
  _Systematics.push_back("CategoryReduction_JET_EffectiveNP_R10_Statistical3");
  _Systematics.push_back("CategoryReduction_JET_EffectiveNP_R10_Statistical4");
  _Systematics.push_back("CategoryReduction_JET_EffectiveNP_R10_Statistical5");
  _Systematics.push_back("CategoryReduction_JET_EffectiveNP_R10_Statistical6");
  _Systematics.push_back("CategoryReduction_JET_EtaIntercalibration_Modelling");
  _Systematics.push_back("CategoryReduction_JET_EtaIntercalibration_NonClosure_2018data");
  _Systematics.push_back("CategoryReduction_JET_EtaIntercalibration_R10_TotalStat");
  _Systematics.push_back("CategoryReduction_JET_Flavor_Composition");
  _Systematics.push_back("CategoryReduction_JET_Flavor_Response");
  _Systematics.push_back("CategoryReduction_JET_LargeR_TopologyUncertainty_V");
  _Systematics.push_back("CategoryReduction_JET_MassRes_WZ_comb");
  _Systematics.push_back("CategoryReduction_JET_MassRes_Top_comb");
  _Systematics.push_back("CategoryReduction_JET_SingleParticle_HighPt");

  /*std::ifstream input_list_file("./systematics-list.txt");
  std::string line;
  if (input_list_file.is_open())
    {
      while ( getline (input_list_file,line) )
        {
          _Systematics.push_back(line);
        }
      input_list_file.close();
    }

  else std::cout << "Unable to open file" << std::endl;
  line = "sh228lund";
  _Systematics.push_back(line);
  line = "sh225";
  _Systematics.push_back(line);*/
 
}

int caliConfig::getIndex(vector<TString> v, TString k){
  int index = -1;
  for (int i=0; i<v.size();i++){
    if(v[i].EqualTo(k)){
      index = i;
      continue;
    }
  }  
  if (index != -1) { 
    std::cout << "Find Index for : " << k << std::endl;
  } 
  else  { 
    cout << "Element" << k << "not found, please chek again" << endl; 
    exit(0);
  }
  return index;
}
