#ifndef CALICONFIG_HEADER
#define CALICONFIG_HEADER

#include "TROOT.h"
#include <iostream>
#include <fstream>
#include <iostream>
#include <string>
#include <map>

using namespace std;

class caliConfig : public TObject{
 public:
  static TString _Calib;
  static bool _doFit;
  static TString _InputDir;
  static TString _TreeName;
  static TString _Category; //lj1 or lj2
  static std::vector<TString> _runProcess;
  //static void initrunProcess();
  static std::vector<TString> _pTBins;
  static std::vector<double> _pTCuts;
  //static void initpTBins();
  static std::vector<TString> _Systematics;
  static void initSystematics();
  static TString _FitOutputDir;
  static TString _XMLOutputDir;
  static int getIndex(vector<TString> v, TString k);  
  ClassDef(caliConfig,1);
};
#endif
  
  

